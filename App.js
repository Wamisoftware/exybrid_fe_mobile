import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {Provider} from 'react-redux';
import {store} from './src/store/store';

import Main from './src/Main';
import Loading from './src/common/Loading';
import Splash from './src/pages/Splash';

const App = () => {
  const [isSplash, setIsSplash] = useState(true);

  useEffect(() => {
    setTimeout(() => setIsSplash(false), 1500);
  }, []);

  return (
    <Provider store={store}>
      {isSplash ? (
        <Splash />
      ) : (
        <>
          <Main />
          <Loading />
        </>
      )}
    </Provider>
  );
};
export default App;
