import React, {useCallback, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as LocaleConfig from 'xdate';
import {getAllAnchors} from '../store/thunks/anchorsThunks';
import {getDateStr} from '../common/helpers';
import MonthArrrow from '../assets/MonthArrow.svg';
import ArrowLeft from '../assets/calendarLeft.svg';
import ArrowRight from '../assets/calendarRight.svg';
import Calendar from '../srcCalendar/calendar';
import {
  setAddPopup,
  setAllAnchors,
  setChangePopup,
  setDataToPopup,
  setExistingPopup,
} from '../store/actions/calendarPopupAction';

const weekdaysShort = ['mon', 'tue', 'wed', 'thu', 'fri'];

LocaleConfig.locales['en'] = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ],
  dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat'],
  today: 'Today',
};
LocaleConfig.defaultLocale = 'en';

const CalendarComponent = () => {
  const dispatch = useDispatch();
  const {anchorsList} = useSelector(state => state.anchors);
  const {employeeData} = useSelector(state => state.employee);
  const {allAnchors} = useSelector(state => state.calendarPopUp);

  const [curMonthDate, setCurMonth] = useState(new Date());
  const [anchorsMarkedDates, setAnchorsMarkedDates] = useState([]);

  useEffect(() => {
    if (employeeData.employeeId) {
      dispatch(getAllAnchors({}));
    }
  }, [curMonthDate, dispatch, employeeData]);

  const filterAnchorsList = useCallback(() => {
    const rez = {};

    for (let i = 0; i < anchorsList.length; ++i) {
      const anchor = anchorsList[i];
      if (
        rez[anchor.date] === undefined ||
        anchor.anchorSourceId === employeeData.emloyeeId
      ) {
        rez[anchor.date] = anchor;
      }
    }

    return Object.values(rez);
  }, [anchorsList, employeeData]);

  useEffect(() => {
    if (anchorsList.length) {
      dispatch(setAllAnchors(filterAnchorsList()));
    }
  }, [anchorsList, filterAnchorsList, dispatch]);

  useEffect(() => {
    if (allAnchors.length) {
      getAnchorsMarkedDates({all: allAnchors});
    }
  }, [allAnchors, getAnchorsMarkedDates]);

  const hidedWeekdays = curdate => {
    const paramDate = new Date(curdate);
    const daysInCurMonth = new Date(
      paramDate.getFullYear(),
      paramDate.getMonth() + 1,
      0,
    ).getDate();
    let weekEnds = [];
    for (let i = 1; i <= daysInCurMonth; i++) {
      let dayNum = new Date(paramDate.setDate(i)).getDay();
      if (dayNum === 6 || dayNum === 0) {
        weekEnds.push({date: getDateStr(new Date(paramDate.setDate(i)))});
      }
    }
    return weekEnds;
  };

  const pastDays = curdate => {
    let past = [];
    if (curdate.getMonth() === new Date().getMonth()) {
      const newD = new Date(curdate);
      for (let i = 1; i < curdate.getDate(); i++) {
        past.push({date: getDateStr(new Date(newD.setDate(i)))});
      }
      return past;
    } else {
      return [];
    }
  };

  const getAnchorsMarkedDates = useCallback(
    ({all}) => {
      let arr = [];
      let hided = hidedWeekdays(curMonthDate);
      let past = pastDays(curMonthDate);
      all.forEach(
        anc =>
          (arr = [
            ...arr,
            {
              date: anc.date,
              customStyles: {
                container: {
                  borderRadius: 0,
                  borderBottomWidth: 4,
                  borderColor:
                    anc.anchorDayType === 'home'
                      ? '#148A07'
                      : anc.anchorDayType === 'office'
                      ? '#00C0EB'
                      : '#9A5FAF',
                  textAlign: anc.anchorExceptionData ? 'center' : 'left',
                },
                text: {
                  color: 'black',
                },
              },
            },
          ]),
      );
      hided.forEach(
        anc =>
          (arr = [
            ...arr,
            {
              date: anc.date,
              customStyles: {
                container: {width: 0},
              },
            },
          ]),
      );
      console.log('past', past);
      console.log('hided', hided);

      past.forEach(
        anc =>
          (arr = [
            ...arr,
            {
              date: anc.date,
              customStyles: {
                container: {borderColor: '#fff'},
                text: {
                  color: '#BDBDBD',
                },
              },
            },
          ]),
      );
      setAnchorsMarkedDates(arr);
    },
    [curMonthDate],
  );

  const handleDayClick = day => {
    const dateStr = getDateStr(new Date(day.timestamp));

    let anchorData = allAnchors.find(item => item.date === dateStr);

    if (!anchorData) {
      dispatch(
        setDataToPopup({date: dateStr, type: 'office', isCohesive: false}),
      );
      dispatch(setAddPopup(true));
    } else if (anchorData.anchorSourceId === employeeData.employeeId) {
      dispatch(setDataToPopup(anchorData));
      dispatch(setChangePopup(true));
    } else {
      dispatch(setDataToPopup(anchorData));
      dispatch(setExistingPopup(true));
    }
  };

  const handleMonthsArrows = route => {
    setCurMonth(prev => {
      const newDate = new Date(prev);
      newDate.setMonth(newDate.getMonth() + (route === 'left' ? -1 : 1));
      return newDate;
    });
  };

  const customHeader = () => {
    return (
      <CalendarHeaderWrap>
        <MonthInfo>
          <MonthTextWrap onPress={() => handleMonthsArrows('right')}>
            <MonthText>
              {LocaleConfig.locales['en'].monthNames[curMonthDate.getMonth()]}
              &nbsp;
              {curMonthDate.getFullYear()}
            </MonthText>
            <MonthArrrow />
          </MonthTextWrap>
          <ArrowsWrap>
            <CalendarArrowWrap onPress={() => handleMonthsArrows('left')}>
              <ArrowLeft />
            </CalendarArrowWrap>
            <CalendarArrowWrap onPress={() => handleMonthsArrows('right')}>
              <ArrowRight />
            </CalendarArrowWrap>
          </ArrowsWrap>
        </MonthInfo>
        <WeekdayWrap>
          {weekdaysShort.map((item, i) => (
            <Weekday key={i}>{item}</Weekday>
          ))}
        </WeekdayWrap>
      </CalendarHeaderWrap>
    );
  };

  return (
    <>
      <SwitchWrap>
        <WrapText>
          <SwitchText>Plan</SwitchText>
        </WrapText>
        <WrapText active={true}>
          <SwitchText active={true}>Anchors</SwitchText>
        </WrapText>
      </SwitchWrap>
      {customHeader()}
      <Calendar
        minDate={new Date()}
        renderHeader={date => null}
        curMonthDate={curMonthDate}
        onDayPress={handleDayClick}
        onDayLongPress={day => {
          console.log('selected day', day);
        }}
        monthFormat={'MMMM yyyy'}
        onMonthChange={month => {
          console.log('month changed', month);
        }}
        onPressArrowLeft={subtractMonth => subtractMonth()}
        onPressArrowRight={addMonth => addMonth()}
        disableAllTouchEventsForDisabledDays={true}
        hideArrows={true}
        hideExtraDays={true}
        firstDay={1}
        theme={{
          textDayFontSize: 20,
          'stylesheet.calendar.header': {
            week: {
              marginTop: 5,
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
          },
          textDisabledColor: '#000000',
        }}
        markingType={'custom'}
        markedDates={[]}
        customMarkedDates={anchorsMarkedDates}
      />
      <LegendWrap>
        <LegendItem>
          <ColorDot color={'#00C0EB'} />
          <LegendName>Office</LegendName>
        </LegendItem>
        <LegendItem>
          <ColorDot color={'#148A07'} />
          <LegendName>Home</LegendName>
        </LegendItem>
        <LegendItem>
          <ColorDot />
          <LegendName>No info</LegendName>
        </LegendItem>
        <LegendItem>
          <ColorDot color={'#9A5FAF'} />
          <LegendName>Day off / travel / sick</LegendName>
        </LegendItem>
        <LegendItem>
          <ColorDot color={'#FF0000'} />
          <LegendName>Conflict</LegendName>
        </LegendItem>
      </LegendWrap>
    </>
  );
};

const CalendarHeaderWrap = styled.View`
  flex: 1;
  align-self: center;
  width: 315px;
  margin: 5px 0;
`;
const MonthInfo = styled.View`
  width: 100%;
  align-self: center;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const WeekdayWrap = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 15px;
`;
const Weekday = styled.Text`
  text-transform: uppercase;
  font-weight: 900;
  font-size: 13px;
  line-height: 18px;
  letter-spacing: -0.078px;
  color: #000000;
`;
const MonthTextWrap = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;
const ArrowsWrap = styled.View`
  flex-direction: row;
  width: 90px;
  justify-content: space-between;
`;
const MonthText = styled.Text`
  font-weight: 900;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.38px;
  color: #000000;
  margin-right: 5px;
`;
const CalendarArrowWrap = styled.TouchableOpacity`
  padding: 0 10px;
`;
const LegendWrap = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0 40px;
`;

const LegendItem = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 30px;
  margin-bottom: 10px;
`;
const ColorDot = styled.View`
  width: 8px;
  height: 8px;
  border-radius: 8px;
  background-color: ${props => props.color || '#DFDFDF'};
`;
const LegendName = styled.Text`
  margin-left: 9px;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.44px;
  color: #263238;
`;
const SwitchWrap = styled.View`
  width: 310px;
  height: 34px;
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  padding: 1px;
  border-radius: 100px;
  align-self: center;
  margin-bottom: 20px;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-top: -15px;
`;
const WrapText = styled.TouchableOpacity`
  width: 152px;
  height: 32px;
  background-color: ${props => (!props.active ? 'transparent' : '#fff')};
  border-radius: 100px;
  justify-content: center;
`;
const SwitchText = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  opacity: 0.55;
  text-align: center;
  color: ${props => (!props.active ? '#24262B' : '#00C0EB')};
`;

export default CalendarComponent;
