import React from 'react';
import styled from 'styled-components/native';
import {useDispatch, useSelector} from 'react-redux';
import {Formik} from 'formik';
import {MainH1, StyledBtn, BtnText} from '../common/styled';
import Logo from '../assets/Logo.svg';
import {getCode} from '../store/thunks/accountThunks';

const LoginPage = props => {
  const {navigation} = props;
  const dispatch = useDispatch();

  return (
    <MainWrap>
      <LoginScroll>
        <LogoBlock>
          <IcoWrap>
            <Logo />
          </IcoWrap>
          <MainH1>Hello!{'\n'}Enter your work email to get started.</MainH1>
        </LogoBlock>
        <Formik
          initialValues={{email: 'bbrutman@outbrain.com'}}
          onSubmit={values => {
            dispatch(
              getCode({
                email: values.email,
                cb: () => navigation.navigate('Enter code'),
              }),
            );
            // navigation.navigate('Enter code');
          }}>
          {({handleChange, handleBlur, handleSubmit, values}) => (
            <>
              <InputsBlock>
                <StyledInput
                  type="email"
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                  placeholder="Work email"
                />
              </InputsBlock>
              <StyledBtn color="#000000" onPress={handleSubmit}>
                <BtnText color="#ffffff">Sign up</BtnText>
              </StyledBtn>
            </>
          )}
        </Formik>
      </LoginScroll>
    </MainWrap>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
  background-color: #ffffff;
`;

const LoginScroll = styled.ScrollView`
  margin: 24px;
`;

const LogoBlock = styled.View`
  display: flex;
`;

const IcoWrap = styled.View`
  padding-left: 90px;
  align-self: center;
  margin-bottom: 45px;
`;

const InputsBlock = styled.View`
  margin: 60px 0 40px;
`;

const StyledInput = styled.TextInput`
  padding: 10px 15px;
  border: 1px solid #babec5;
  border-radius: 4px;
  font-size: 16px;
  margin-bottom: 30px;
`;

export default LoginPage;
