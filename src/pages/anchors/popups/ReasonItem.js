import React from 'react';
import styled from 'styled-components/native';

const ReasonItem = props => {
  const {text, isChecked, id, handleCheckInterest} = props;
  return (
    <StyledTag isChecked={isChecked} onPress={() => handleCheckInterest(id)}>
      <TagText isChecked={isChecked}>{text}</TagText>
    </StyledTag>
  );
};

const StyledTag = styled.TouchableOpacity`
  padding: 3px 6px;
  background-color: ${props => (props.isChecked ? '#00C0EB' : 'transparent')};
  border: 1px solid #00c0eb;
  border-radius: 12px;
  margin: 0 5px 10px 0;
`;
const TagText = styled.Text`
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: ${props => (props.isChecked ? '#ffffff' : '#666666')};
`;

export default ReasonItem;
