import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {useDispatch, useSelector} from 'react-redux';
import {View, Switch, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {StyledBtn, BtnText, PopupText, DropText} from '../../../common/styled';
import ArrowR from '../../../assets/requestArrowR.svg';
import ArrowD from '../../../assets/requestArrowD.svg';
import ReasonItem from './ReasonItem';
import {
  requestAnchorException,
  getExceptionReasons,
  cancelRequestException,
} from '../../../store/thunks/anchorsThunks';
import {getDateStr} from '../../../common/helpers';

const styles = StyleSheet.create({
  flex5: {flex: 40, textAlign: 'center'},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
  blurWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  blur: {
    zIndex: 10,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  mr5: {marginRight: 5},
  mb15: {marginBottom: 15, width: 312},
});

const weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const ExistingAnchorPopup = props => {
  const {data, close} = props;
  const date = new Date(data.date);

  const dispatch = useDispatch();
  const {employeeData} = useSelector(state => state.employee);
  const {exceptionReasons} = useSelector(state => state.anchors);
  const {allColleagues} = useSelector(state => state.colleagues);
  let anchorSourceName;
  allColleagues.some(item => {
    if (item.id === data.anchorSourceId) {
      anchorSourceName = `${item.firstName} ${item.lastName}`;
    }
    return item.id === data.anchorSourceId;
  });

  const dateStr = getDateStr(date);

  const [type, setType] = useState('');
  const [toogle, setToggle] = useState(false);
  const [isDropDown, setIsDropDown] = useState(false);
  const [allReasons, setAllReasons] = useState({});
  const [newReasonValue, setNewReasontValue] = useState('');

  useEffect(() => {
    dispatch(getExceptionReasons({}));
  }, [dispatch]);

  useEffect(() => {
    if (type !== 'office') {
      setToggle(false);
    }
  }, [type]);

  useEffect(() => {
    if (!type) {
      if (data && data.anchorExceptionData) {
        setType(data.anchorExceptionData.requestedDayType);
      } else if (data && data.anchorDayType) {
        data.anchorDayType === 'office'
          ? setType('home')
          : setType(data.anchorDayType);
      }
    }
    if (data && data.isCohesive) {
      setToggle(data.isCohesive);
    }
    if (exceptionReasons.home && !allReasons.home) {
      let accList = {};
      for (let key in exceptionReasons) {
        accList[key] = exceptionReasons[key].map(item => ({
          text: item,
          isChecked: data.anchorExceptionData
            ? !!data.anchorExceptionData.reasons.includes(item)
            : false,
          id: +(Math.random() * 1000000).toFixed(),
        }));
      }
      data.anchorExceptionData?.reasons.forEach(res => {
        if (
          !exceptionReasons[data.anchorExceptionData.requestedDayType].includes(
            res,
          )
        ) {
          accList[data.anchorExceptionData.requestedDayType].push({
            text: res,
            isChecked: true,
            id: +(Math.random() * 1000000).toFixed(),
          });
        }
      });
      setAllReasons(accList);
    }
  }, [data, exceptionReasons]);

  const handleRequestException = () => {
    console.log('handleRequestException', {
      companyId: employeeData.companyId,
      employeeId: employeeData.employeeId,
      managerId: employeeData.directManagerId,
      teamId: employeeData.teamId,
      date: dateStr,
      anchorDayType: data.anchorDayType,
      exceptionData: {
        requestedDayType: type,
        reasons: allReasons[type].reduce((acc, cur) => {
          cur.isChecked && acc.push(cur.text);
          return acc;
        }, []),
      },
    });
    dispatch(
      requestAnchorException({
        companyId: employeeData.companyId,
        employeeId: employeeData.employeeId,
        managerId: employeeData.directManagerId,
        teamId: employeeData.teamId,
        date: dateStr,
        anchorDayType: data.anchorDayType,
        exceptionData: {
          requestedDayType: type,
          reasons: allReasons[type].reduce((acc, cur) => {
            cur.isChecked && acc.push(cur.text);
            return acc;
          }, []),
        },
      }),
    );
    close();
  };

  const handleCancelRequest = () => {
    dispatch(
      cancelRequestException({
        companyId: employeeData.companyId,
        employeeId: employeeData.employeeId,
        managerId: employeeData.directManagerId,
        teamId: employeeData.teamId,
        date: dateStr,
        anchorDayType: data.anchorDayType,
      }),
    );
    close();
  };

  return (
    <>
      <View style={styles.blurWrap}>
        <TouchableOpacity style={styles.blur} onPress={close} />
      </View>
      <SettingContainer>
        <Question>Existing anchor</Question>
        <PopupText style={styles.mb15}>
          On {weekdays[date.getDay()]}, {monthNames[date.getMonth()]}{' '}
          {date.getDate()}
          <TextBold> {anchorSourceName}</TextBold> {'\n'}asked that you work
          from the
          <TextBold> {data.anchorDayType}.</TextBold>
        </PopupText>
        {employeeData.managedTeamId ? (
          <ToggleWrap>
            <Switch
              disabled={type !== 'office'}
              trackColor={{false: '#e9e9eb', true: '#34C759'}}
              thumbColor={'#fff'}
              onValueChange={() => setToggle(prev => !prev)}
              value={toogle}
            />
            <ToggleText dis={type !== 'office'}>
              {toogle ? 'Remove from ' : 'Add to '}my direct reports
            </ToggleText>
          </ToggleWrap>
        ) : null}

        <BtnsWrap>
          <StyledBtn color="#000000" onPress={close}>
            <BtnText color="#ffffff">OK</BtnText>
          </StyledBtn>
        </BtnsWrap>
        <RequestWrap onPress={() => setIsDropDown(prev => !prev)}>
          {isDropDown ? (
            <ArrowD style={styles.mr5} />
          ) : (
            <ArrowR style={styles.mr5} />
          )}
          <DropText>
            {data?.anchorExceptionData
              ? 'You have requested an exception'
              : 'Request exception'}
          </DropText>
        </RequestWrap>
        {isDropDown ? (
          <>
            <SwitchWrap>
              <WrapText
                disabled={true}
                active={type === 'office'}
                onPress={() => setType('office')}>
                <SwitchText active={type === 'office'}>Office</SwitchText>
              </WrapText>
              <WrapText
                active={type === 'home'}
                onPress={() => setType('home')}>
                <SwitchText active={type === 'home'}>Home</SwitchText>
              </WrapText>
              <WrapText
                active={type === 'travel'}
                onPress={() => setType('travel')}>
                <SwitchText active={type === 'travel'}>Travel</SwitchText>
              </WrapText>
              <WrapText
                active={type === 'dayoff'}
                onPress={() => setType('dayoff')}>
                <SwitchText active={type === 'dayoff'}>Day off</SwitchText>
              </WrapText>
            </SwitchWrap>
            <AddWrap>
              <StyledINput
                onChangeText={setNewReasontValue}
                value={newReasonValue}
              />
              <AddBtn
                onPress={() => {
                  if (newReasonValue.trim()) {
                    setAllReasons(prev => {
                      const newPrev = Object.assign({}, prev);
                      const newReasons = [
                        ...prev[type],
                        {
                          text: newReasonValue.trim(),
                          isChecked: true,
                          id: +(Math.random() * 10000).toFixed(),
                        },
                      ];
                      newPrev[type] = newReasons;
                      return newPrev;
                    });
                  }
                  setNewReasontValue('');
                }}>
                <Text>Add</Text>
              </AddBtn>
            </AddWrap>
            <TagsWrap>
              {allReasons[type]?.map(({text, id, isChecked}) => (
                <ReasonItem
                  text={text}
                  isChecked={isChecked}
                  key={id}
                  id={id}
                  handleCheckInterest={ID => {
                    setAllReasons(prev => {
                      const newPrev = Object.assign({}, prev);
                      const newReasons = prev[type].map(item => {
                        if (item.id === ID) {
                          return {...item, isChecked: !item.isChecked};
                        } else {
                          return item;
                        }
                      });
                      newPrev[type] = newReasons;
                      return newPrev;
                    });
                  }}
                />
              ))}
            </TagsWrap>
            <BtnsWrap>
              <StyledBtn
                color="#ffffff"
                border={true}
                onPress={() => {
                  if (data?.anchorExceptionData) {
                    handleCancelRequest();
                  } else if (data) {
                    handleRequestException();
                  }
                }}>
                <BtnText color="#000000">
                  {data?.anchorExceptionData
                    ? 'Cancel request'
                    : 'Request exception'}
                </BtnText>
              </StyledBtn>
            </BtnsWrap>
          </>
        ) : null}
      </SettingContainer>
    </>
  );
};

const SettingContainer = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 11;
  background-color: #fff;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  padding: 25px 35px;
  display: flex;
  align-items: center;
`;
const Question = styled.Text`
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin: 20px auto;
`;

const SwitchWrap = styled.View`
  width: 312px;
  height: 24px;
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  padding: 1px;
  border-radius: 100px;
  align-self: center;
  margin: 15px 0;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;
const WrapText = styled.TouchableOpacity`
  width: 25%;
  height: 21px;
  background-color: ${props => (!props.active ? 'transparent' : '#fff')};
  border-radius: 100px;
  justify-content: center;
`;
const SwitchText = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  opacity: 0.55;
  text-align: center;
  color: ${props => (!props.active ? '#24262B' : '#00C0EB')};
`;

const BtnsWrap = styled.View`
  width: 312px;
`;

const ToggleWrap = styled.View`
  width: 312px;
  flex-direction: row;
  /* margin-top: 15px; */
  margin-bottom: 15px;
  align-items: center;
`;
const ToggleText = styled.Text`
  margin-left: 15px;
  font-size: 16px;
  line-height: 19px;
  text-align: center;
  color: ${props => (props.dis ? '#bdbdbd' : '#000000')};
`;
const RequestWrap = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  /* padding-left: 10px; */
`;
const TagsWrap = styled.View`
  width: 312px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 10px;
  /* padding: 25px 20px; */
`;

const AddWrap = styled.View`
  display: flex;
  flex-direction: row;
  width: 312px;
  margin-bottom: 15px;
`;
const StyledINput = styled.TextInput`
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  border-radius: 8px;
  padding: 0 16px;
  flex: 5;
  font-weight: 500;
  font-size: 16px;
`;

const AddBtn = styled.TouchableOpacity`
  padding: 0 15px;
  border: 1px solid #000000;
  border-radius: 26px;
  margin-left: 15px;
  flex: 1;
  align-items: center;
  justify-content: center;
`;
const TextBold = styled.Text`
  font-weight: bold;
`;
export default ExistingAnchorPopup;
