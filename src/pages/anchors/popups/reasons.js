export const reasons = {
  dayOff: [
    {text: 'Vacation', isChecked: false, id: 1},
    {text: 'Vacation abroad', isChecked: false, id: 2},
    {text: 'Sick leave', isChecked: false, id: 3},
    {text: 'Maternity/paternity', isChecked: false, id: 4},
    {text: 'Sick child', isChecked: false, id: 5},
  ],
  travel: [
    {text: 'Business travel', isChecked: false, id: 6},
    {text: 'Preparations', isChecked: false, id: 7},
    {text: 'Arriving from abroad', isChecked: false, id: 8},
    {text: 'Visa / permits', isChecked: false, id: 9},
  ],
  home: [
    {text: 'Personal issue', isChecked: false, id: 10},
    {text: 'Health issue', isChecked: false, id: 11},
    {text: 'Family event', isChecked: false, id: 12},
    {text: 'Parents / elderly', isChecked: false, id: 13},
    {text: 'School event', isChecked: false, id: 14},
    {text: 'Errands', isChecked: false, id: 15},
    {text: 'Caring for kids', isChecked: false, id: 16},
  ],
  office: [
    {text: 'Client meeting', isChecked: false, id: 17},
    {text: 'Important visit', isChecked: false, id: 18},
    {text: 'Project sprint', isChecked: false, id: 19},
    {text: 'Employee onboarding', isChecked: false, id: 20},
    {text: 'All hands', isChecked: false, id: 21},
    {text: 'Team metting', isChecked: false, id: 22},
    {text: 'Equipment', isChecked: false, id: 23},
  ],
};
