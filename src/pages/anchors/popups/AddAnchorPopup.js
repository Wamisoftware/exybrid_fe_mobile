import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {useDispatch, useSelector} from 'react-redux';
import {View, Switch, StyleSheet, TouchableOpacity} from 'react-native';
import {StyledBtn, BtnText, PopupText} from '../../../common/styled';
import {addAnchor} from '../../../store/thunks/anchorsThunks';
import {getDateStr} from '../../../common/helpers';

const styles = StyleSheet.create({
  flex5: {flex: 40, textAlign: 'center'},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
  blurWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  blur: {
    zIndex: 10,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
});

const weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const AddAnchorPopup = props => {
  const {data, close} = props;
  const date = new Date(data.date);
  const dateStr = getDateStr(date);

  const dispatch = useDispatch();
  const {employeeData} = useSelector(state => state.employee);

  const [type, setType] = useState('office');
  const [toogle, setToggle] = useState(false);

  useEffect(() => {
    if (type !== 'office') {
      setToggle(false);
    }
  }, [type]);

  const handleAddAnchor = () => {
    dispatch(
      addAnchor({
        companyId: employeeData.companyId,
        employeeId: employeeData.employeeId,
        managerId: employeeData.directManagerId,
        teamId: employeeData.teamId,
        managedTeamId: employeeData.managedTeamId,
        date: dateStr,
        anchorDayType: type,
        isCohesive: toogle,
        directReports: employeeData.directReports,
      }),
    );
    close();
  };

  return (
    <>
      <View style={styles.blurWrap}>
        <TouchableOpacity style={styles.blur} onPress={close} />
      </View>
      <SettingContainer>
        <Question>Add anchor</Question>
        <PopupText>
          Where would you like to be on {'\n'}
          {weekdays[date.getDay()]}, {monthNames[date.getMonth()]}{' '}
          {date.getDate()}:
        </PopupText>
        <SwitchWrap>
          <WrapText
            active={type === 'office'}
            onPress={() => setType('office')}>
            <SwitchText active={type === 'office'}>Office</SwitchText>
          </WrapText>
          <WrapText active={type === 'home'} onPress={() => setType('home')}>
            <SwitchText active={type === 'home'}>Home</SwitchText>
          </WrapText>
          <WrapText
            active={type === 'travel'}
            onPress={() => setType('travel')}>
            <SwitchText active={type === 'travel'}>Travel</SwitchText>
          </WrapText>
          <WrapText
            active={type === 'dayoff'}
            onPress={() => setType('dayoff')}>
            <SwitchText active={type === 'dayoff'}>Day off</SwitchText>
          </WrapText>
        </SwitchWrap>
        <BtnsWrap>
          <StyledBtn color="#000000" onPress={handleAddAnchor}>
            <BtnText color="#ffffff">Add</BtnText>
          </StyledBtn>
        </BtnsWrap>
        {employeeData.managedTeamId ? (
          <ToggleWrap>
            <Switch
              disabled={type !== 'office'}
              trackColor={{false: '#e9e9eb', true: '#34C759'}}
              thumbColor={'#fff'}
              onValueChange={() => setToggle(prev => !prev)}
              value={toogle}
            />
            <ToggleText dis={type !== 'office'}>
              {toogle ? 'Remove from ' : 'Add to '}my direct reports
            </ToggleText>
          </ToggleWrap>
        ) : null}
      </SettingContainer>
    </>
  );
};

const SettingContainer = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 11;
  background-color: #fff;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  padding: 25px 35px;
  display: flex;
  align-items: center;
`;
const Question = styled.Text`
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin: 20px auto;
`;

const SwitchWrap = styled.View`
  width: 310px;
  height: 48px;
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  padding: 1px;
  border-radius: 100px;
  align-self: center;
  margin: 35px 0;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;
const WrapText = styled.TouchableOpacity`
  width: 78px;
  height: 45px;
  background-color: ${props => (!props.active ? 'transparent' : '#fff')};
  border-radius: 100px;
  justify-content: center;
`;
const SwitchText = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  opacity: 0.55;
  text-align: center;
  color: ${props => (!props.active ? '#24262B' : '#00C0EB')};
`;

const BtnsWrap = styled.View`
  width: 100%;
`;

const ToggleWrap = styled.View`
  width: 100%;
  flex-direction: row;
  margin-bottom: 25px;
  align-items: center;
`;
const ToggleText = styled.Text`
  margin-left: 15px;
  font-size: 16px;
  line-height: 19px;
  text-align: center;
  color: ${props => (props.dis ? '#bdbdbd' : '#000000')};
`;
export default AddAnchorPopup;
