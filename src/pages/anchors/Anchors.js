import React, {useCallback, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as LocaleConfig from 'xdate';
import Calendar from '../../srcCalendar/calendar';
import MonthArrrow from '../../assets/MonthArrow.svg';
import {MainH1, StyledBtn, BtnText, Empty} from '../../common/styled';
import Arrow from '../../assets/arrowLeft.svg';
import ArrowLeft from '../../assets/calendarLeft.svg';
import ArrowRight from '../../assets/calendarRight.svg';
import AnchorIcon from '../../assets/Anchor.svg';
import AddAnchorPopup from './popups/AddAnchorPopup';
import ChangeAnchorPopup from './popups/ChangeAnchorPopup';
import ExistingAnchorPopup from './popups/ExistingAnchorsPopup';
import {getAllAnchors} from '../../store/thunks/anchorsThunks';
import {getEmployeeData} from '../../store/thunks/employeeThunks';
import {getDateStr} from '../../common/helpers';
import {getAllColleagues} from '../../store/thunks/colleaguesThunks';
import CalendarComponent from '../../components/CalendarComponent';

const styles = StyleSheet.create({
  flex5: {flex: 40, textAlign: 'center'},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
  blurWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  blur: {
    zIndex: 10,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
});

const weekdaysShort = ['mon', 'tue', 'wed', 'thu', 'fri'];

LocaleConfig.locales['en'] = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ],
  dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat'],
  today: 'Today',
};
LocaleConfig.defaultLocale = 'en';

const Anchors = ({navigation}) => {
  const dispatch = useDispatch();
  const {anchorsList} = useSelector(state => state.anchors);
  const {employeeData} = useSelector(state => state.employee);

  const [curMonthDate, setCurMonth] = useState(new Date());
  const [btns, setBtns] = useState(true);
  const [addPopup, setAddPopup] = useState(false);
  const [changePopup, setChangePopup] = useState(false);
  const [existingPopup, setExistingPopup] = useState(false);
  const [dataToPopup, setDataToPopup] = useState();
  const [anchorsMarkedDates, setAnchorsMarkedDates] = useState([]);

  const [allAnchors, setAllAnchors] = useState([]);

  useEffect(() => {
    // dispatch(getAllColleagues());
    // dispatch(getEmployeeData());
  }, [dispatch]);

  useEffect(() => {
    if (employeeData.employeeId) {
      dispatch(getAllAnchors({}));
      hidedWeekdays(curMonthDate);
    }
  }, [curMonthDate, dispatch, employeeData]);

  const filterAnchorsList = useCallback(() => {
    const rez = {};

    for (let i = 0; i < anchorsList.length; ++i) {
      const anchor = anchorsList[i];
      if (
        rez[anchor.date] === undefined ||
        anchor.anchorSourceId === employeeData.emloyeeId
      ) {
        rez[anchor.date] = anchor;
      }
    }

    return Object.values(rez);
  }, [anchorsList, employeeData]);

  useEffect(() => {
    if (anchorsList.length) {
      setAllAnchors(filterAnchorsList());
    }
  }, [anchorsList, filterAnchorsList]);

  useEffect(() => {
    employeeData.employeeId && console.log('employeeData', employeeData);
  }, [employeeData]);

  useEffect(() => {
    getAnchorsMarkedDates({all: allAnchors});
  }, [allAnchors]);

  const hidedWeekdays = curdate => {
    const paramDate = new Date(curdate);
    const daysInCurMonth = new Date(
      paramDate.getFullYear(),
      paramDate.getMonth() + 1,
      0,
    ).getDate();
    let weekEnds = [];
    for (let i = 1; i <= daysInCurMonth; i++) {
      let dayNum = new Date(paramDate.setDate(i)).getDay();
      if (dayNum === 6 || dayNum === 0) {
        weekEnds.push(new Date(paramDate.setDate(i)));
      }
    }
    // console.log('weekEnds', weekEnds);
  };

  const getAnchorsMarkedDates = ({all}) => {
    let arr = [];
    all.forEach(
      anc =>
        (arr = [
          ...arr,
          {
            date: anc.date,
            customStyles: {
              container: {
                borderRadius: 0,
                borderBottomWidth: 4,
                borderColor:
                  anc.anchorDayType === 'home'
                    ? '#148A07'
                    : anc.anchorDayType === 'office'
                    ? '#00C0EB'
                    : '#9A5FAF',
                textAlign: anc.anchorExceptionData ? 'center' : 'left',
              },
              text: {
                color: 'black',
              },
            },
          },
        ]),
    );
    // let a = {
    //   '2021-05-16': {
    //     customStyles: {
    //       container: {width: 0},
    //     },
    //   },
    // };
    setAnchorsMarkedDates(arr);
  };

  const handleDayClick = day => {
    const dateStr = getDateStr(new Date(day.timestamp));

    let anchorData = allAnchors.find(item => item.date === dateStr);

    if (!anchorData) {
      setDataToPopup({date: dateStr, type: 'office', isCohesive: false});
      setAddPopup(true);
    } else if (anchorData.anchorSourceId === employeeData.employeeId) {
      setDataToPopup(anchorData);
      setChangePopup(true);
    } else {
      setDataToPopup(anchorData);
      setExistingPopup(true);
    }
  };

  const handleMonthsArrows = route => {
    setCurMonth(prev => {
      const newDate = new Date(prev);
      newDate.setMonth(newDate.getMonth() + (route === 'left' ? -1 : 1));
      return newDate;
    });
  };

  const customHeader = () => {
    return (
      <CalendarHeaderWrap>
        <MonthInfo>
          <MonthTextWrap onPress={() => handleMonthsArrows('right')}>
            <MonthText>
              {LocaleConfig.locales['en'].monthNames[curMonthDate.getMonth()]}
              &nbsp;
              {curMonthDate.getFullYear()}
            </MonthText>
            <MonthArrrow />
          </MonthTextWrap>
          <ArrowsWrap>
            <CalendarArrowWrap onPress={() => handleMonthsArrows('left')}>
              <ArrowLeft />
            </CalendarArrowWrap>
            <CalendarArrowWrap onPress={() => handleMonthsArrows('right')}>
              <ArrowRight />
            </CalendarArrowWrap>
          </ArrowsWrap>
        </MonthInfo>
        <WeekdayWrap>
          {weekdaysShort.map((item, i) => (
            <Weekday key={i}>{item}</Weekday>
          ))}
        </WeekdayWrap>
      </CalendarHeaderWrap>
    );
  };

  return (
    <>
      <MainWrap>
        <Colored>
          <ColoredBackGround />
        </Colored>
        <MainScroll>
          <Head>
            <TopWrap>
              <ArrowWrap>
                <Arrow />
              </ArrowWrap>
              <MainH1 style={styles.flex5}>Any Anchors?</MainH1>
            </TopWrap>
            <IconWrap>
              <AnchorIcon />
            </IconWrap>

            <StyledText>
              We will build you the optimal schedule. {'\n'}In case you have
              dates with limited flexibility, click them below to let us know.
            </StyledText>
          </Head>
          <SwitchWrap>
            <WrapText>
              <SwitchText>Plan</SwitchText>
            </WrapText>
            <WrapText active={true}>
              <SwitchText active={true}>Anchors</SwitchText>
            </WrapText>
          </SwitchWrap>
          {customHeader()}
          <Calendar
            minDate={new Date()}
            renderHeader={date => null}
            curMonthDate={curMonthDate}
            onDayPress={handleDayClick}
            onDayLongPress={day => {
              console.log('selected day', day);
            }}
            monthFormat={'MMMM yyyy'}
            onMonthChange={month => {
              console.log('month changed', month);
            }}
            onPressArrowLeft={subtractMonth => subtractMonth()}
            onPressArrowRight={addMonth => addMonth()}
            disableAllTouchEventsForDisabledDays={true}
            hideArrows={true}
            hideExtraDays={true}
            firstDay={1}
            theme={{
              textDayFontSize: 20,
              'stylesheet.calendar.header': {
                week: {
                  marginTop: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                },
              },
              textDisabledColor: '#000000',
            }}
            markingType={'custom'}
            markedDates={[]}
            customMarkedDates={anchorsMarkedDates}
          />
          <LegendWrap>
            <LegendItem>
              <ColorDot color={'#00C0EB'} />
              <LegendName>Office</LegendName>
            </LegendItem>
            <LegendItem>
              <ColorDot color={'#148A07'} />
              <LegendName>Home</LegendName>
            </LegendItem>
            <LegendItem>
              <ColorDot />
              <LegendName>No info</LegendName>
            </LegendItem>
            <LegendItem>
              <ColorDot color={'#9A5FAF'} />
              <LegendName>Day off / travel / sick</LegendName>
            </LegendItem>
            <LegendItem>
              <ColorDot color={'#FF0000'} />
              <LegendName>Conflict</LegendName>
            </LegendItem>
          </LegendWrap>
          <Empty />
        </MainScroll>
      </MainWrap>
      {btns ? (
        <BtnsWrap style={styles.borderTop}>
          <StyledBtn
            color="#000000"
            onPress={() => navigation.navigate('Interests')}>
            <BtnText color="#ffffff">
              {allAnchors.length ? 'OK' : 'No anchors - I’m good!'}
            </BtnText>
          </StyledBtn>
        </BtnsWrap>
      ) : null}

      {addPopup && (
        <AddAnchorPopup
          data={dataToPopup}
          close={() => setAddPopup(false)}
          setAllAnchors={setAllAnchors}
        />
      )}
      {changePopup && (
        <ChangeAnchorPopup
          data={dataToPopup}
          close={() => setChangePopup(false)}
          setAllAnchors={setAllAnchors}
        />
      )}
      {existingPopup && (
        <ExistingAnchorPopup
          data={dataToPopup}
          close={() => setExistingPopup(false)}
          setAllAnchors={setAllAnchors}
        />
      )}
    </>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
  position: relative;
`;
const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #ffffff;
`;

const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const MainScroll = styled.ScrollView`
  width: 100%;
  flex: 1;
  background-color: #ffffff;
  padding-bottom: 80px;
`;
const BtnsWrap = styled.View`
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 3;
  justify-content: flex-end;
  width: 100%;
  padding: 16px 32px 0;
  background-color: #ffffff;
`;

const Head = styled.View`
  padding-top: 20px;
  width: 100%;
  background-color: #ffffff;
`;
const ArrowWrap = styled.TouchableOpacity`
  position: absolute;
  top: -2px;
  left: 20px;
  flex: 1;
  z-index: 2;
  padding: 10px 0;
  margin-bottom: 20px;
`;
const TopWrap = styled.View`
  padding: 0 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
`;
const StyledText = styled.Text`
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;

  color: #666666;
  margin: 20px 20px 20px 30px;
`;

const IconWrap = styled.View`
  align-self: center;
`;

const CalendarHeaderWrap = styled.View`
  flex: 1;
  align-self: center;
  width: 315px;
`;
const MonthInfo = styled.View`
  width: 100%;
  align-self: center;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const WeekdayWrap = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 15px;
`;
const Weekday = styled.Text`
  text-transform: uppercase;
  font-weight: 900;
  font-size: 13px;
  line-height: 18px;
  letter-spacing: -0.078px;
  color: #000000;
`;
const MonthTextWrap = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  /* margin-left: 15px; */
`;
const ArrowsWrap = styled.View`
  flex-direction: row;
  width: 90px;
  justify-content: space-between;
`;
const MonthText = styled.Text`
  font-weight: 900;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.38px;
  color: #000000;
  margin-right: 5px;
`;
const CalendarArrowWrap = styled.TouchableOpacity`
  padding: 0 10px;
`;
const LegendWrap = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0 40px;
`;

const LegendItem = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 30px;
  margin-bottom: 10px;
`;
const ColorDot = styled.View`
  width: 8px;
  height: 8px;
  border-radius: 8px;
  background-color: ${props => props.color || '#DFDFDF'};
`;
const LegendName = styled.Text`
  margin-left: 9px;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.44px;
  color: #263238;
`;
const SwitchWrap = styled.View`
  width: 310px;
  height: 34px;
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  padding: 1px;
  border-radius: 100px;
  align-self: center;
  margin-bottom: 20px;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;
const WrapText = styled.TouchableOpacity`
  width: 152px;
  height: 32px;
  background-color: ${props => (!props.active ? 'transparent' : '#fff')};
  border-radius: 100px;
  justify-content: center;
`;
const SwitchText = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  opacity: 0.55;
  text-align: center;
  color: ${props => (!props.active ? '#24262B' : '#00C0EB')};
`;
export default Anchors;
