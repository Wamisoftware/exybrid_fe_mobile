import React from 'react';
import styled from 'styled-components/native';
import SplashIcon from '../assets/splash.svg';
import {MainH1} from '../common/styled';

const Splash = () => {
  return (
    <SplashWrap>
      <SplashIcon />
      <MainH1 style={{marginTop: 15}}>
        Connect with the {'\n'}people you want.
      </MainH1>
    </SplashWrap>
  );
};

const SplashWrap = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export default Splash;
