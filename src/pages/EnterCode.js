import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {useDispatch, useSelector} from 'react-redux';
import {StyleSheet} from 'react-native';
import {MainH1} from '../common/styled';
import Logo from '../assets/Logo.svg';
import {signInWithCode} from '../store/thunks/accountThunks';

const styles = StyleSheet.create({
  borderBot: {
    borderBottomWidth: 1,
    borderColor: '#BABEC5',
  },
});

const EnterCode = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = useState('');

  // navigation.addListener('beforeRemove', e => {
  //   e.preventDefault();
  // });

  useEffect(() => {
    if (inputValue.length === 4) {
      dispatch(
        signInWithCode({
          code: inputValue,
          cb: () => navigation.navigate('ConfirmTeam'),
        }),
      );
      // navigation.navigate('ConfirmTeam');
    }
  }, [inputValue, navigation, dispatch]);

  return (
    <MainWrap>
      <LoginScroll>
        <LogoBlock>
          <IcoWrap>
            <Logo />
          </IcoWrap>
          <MainH1>Please enter the 4-digit{'\n'} code you received</MainH1>
        </LogoBlock>
        <CodeInput
          style={styles.borderBot}
          value={inputValue}
          onChangeText={setInputValue}
          keyboardType="numeric"
        />
      </LoginScroll>
    </MainWrap>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
  background-color: #ffffff;
`;

const LoginScroll = styled.ScrollView`
  margin: 24px;
`;

const LogoBlock = styled.View`
  display: flex;
`;

const IcoWrap = styled.View`
  padding-left: 90px;
  align-self: center;
  margin-bottom: 45px;
`;

const CodeInput = styled.TextInput`
  padding: 10px 15px;
  font-size: 46px;
  margin-top: 20px;
  margin-bottom: 30px;
  width: 250px;
  align-self: center;
  text-align: center;
`;

export default EnterCode;
