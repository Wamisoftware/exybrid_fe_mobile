import React from 'react';
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {MainH1, StyledBtn, BtnText} from '../common/styled';

const styles = StyleSheet.create({
  textCenter: {textAlign: 'center'},
});

const YouReAllSet = ({navigation}) => {
  return (
    <MainWrap>
      <Colored>
        <ColoredBackGround />
      </Colored>
      <BoxWrap>
        <MainH1 style={styles.textCenter}>That’s it, you’re all set!</MainH1>
        <BtnsWrap>
          <StyledBtn
            color="#000000"
            onPress={() => navigation.navigate('HomePage')}>
            <BtnText color="#ffffff">Continue</BtnText>
          </StyledBtn>
        </BtnsWrap>
      </BoxWrap>
    </MainWrap>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
  position: relative;
  justify-content: center;
`;
const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #ffffff;
`;

const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const BoxWrap = styled.View`
  height: 380px;
  padding: 16px;
  margin: 0 16px;
  background-color: #f5f5f5;
  border-radius: 20px;
  justify-content: space-around;
`;
const BtnsWrap = styled.View`
  width: 100%;
`;

export default YouReAllSet;
