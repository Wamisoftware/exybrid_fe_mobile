import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getEmployeeData} from '../store/thunks/employeeThunks';
import User from '../assets/default-user.png';
import {
  MainH1,
  NormalGrayText,
  NormalText,
  StyledBtn,
  BtnText,
  GrayText,
} from '../common/styled';

const styles = StyleSheet.create({
  shadow: {
    width: 155,
    height: 155,
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 155,
    overflow: 'hidden',
    marginTop: 20,
    marginBottom: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  image: {
    // zIndex: 200,
    width: 152,
    height: 152,
  },
  blurWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  blur: {
    zIndex: 10,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  marginH: {
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 40,
    textAlign: 'center',
  },
  centered: {
    marginTop: 10,
    marginBottom: 25,
    alignItems: 'center',
  },
});

const ConfirmTeam = props => {
  const dispatch = useDispatch();

  const {navigation} = props;
  const {employeeData} = useSelector(state => state.employee);
  const [isWrongTeamPopup, setIsWrongTeamPopup] = useState(false);

  useEffect(() => {
    dispatch(getEmployeeData({}));
  }, [dispatch]);

  // useEffect(() => {
  //   console.log('--------UseEffect employeeData', employeeData);
  // }, [employeeData]);

  return (
    <>
      <MainWrap>
        <Colored>
          <ColoredBackGround />
          <WhiteBackGround />
        </Colored>
        <MainScroll>
          <Content>
            <MainH1>Good Morning!</MainH1>
            <View style={styles.shadow}>
              <Image
                style={styles.image}
                source={
                  employeeData.employeeImageLink
                    ? {uri: employeeData.employeeImageLink}
                    : User
                }
              />
            </View>
            <Name>{employeeData.employeeName}</Name>
            <NormalText>Please confirm your team and manager:</NormalText>
            <Info>
              <NormalGrayText>Team</NormalGrayText>
              <MainH1>{employeeData.teamName}</MainH1>
              <NormalGrayText>Manager</NormalGrayText>
              <MainH1>{employeeData.directManagerName}</MainH1>
            </Info>
            <BtnsWrap>
              <StyledBtn
                color="#000000"
                onPress={() => navigation.navigate('Friends')}>
                <BtnText color="#ffffff">Confirm</BtnText>
              </StyledBtn>
              <StyledBtn
                color="#ffffff"
                border={true}
                onPress={() => setIsWrongTeamPopup(true)}>
                <BtnText color="#000000">Wrong team?</BtnText>
              </StyledBtn>
            </BtnsWrap>
          </Content>
        </MainScroll>
      </MainWrap>
      {isWrongTeamPopup && (
        <>
          <View style={styles.blurWrap}>
            <View style={styles.blur} onPress={() => {}} />
          </View>
          <WrongContainer>
            <Question>Wrong team?</Question>
            <GrayText style={styles.marginH}>
              Click below to notify us and we will make sure to move you to the
              right team.
            </GrayText>
            <BtnsWrap>
              <StyledBtn
                color="#000000"
                onPress={() => navigation.navigate('Friends')}>
                <BtnText color="#ffffff">Send notification</BtnText>
              </StyledBtn>
              <TouchableOpacity
                style={styles.centered}
                onPress={() => setIsWrongTeamPopup(false)}>
                <BtnText color="#000000">Back</BtnText>
              </TouchableOpacity>
            </BtnsWrap>
          </WrongContainer>
        </>
      )}
    </>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
`;
const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #96e1f2;
`;
const WhiteBackGround = styled.View`
  flex: 5;
  background-color: #ffffff;
`;
const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const MainScroll = styled.ScrollView`
  width: 100%;
  flex: 1;
`;
const Content = styled.View`
  margin: 20px 24px 0;
  flex: 1;
  align-items: center;
`;

const Name = styled.Text`
  font-size: 30px;
  line-height: 36px;
  font-weight: 600;
  text-align: center;
  color: #000000;
  margin-bottom: 10px;
`;
const Info = styled.View`
  width: 100%;
  margin: 40px 0;
`;
const BtnsWrap = styled.View`
  width: 100%;
`;
const WrongContainer = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 11;
  background-color: #fff;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  padding: 25px;
  display: flex;
  align-items: center;
`;
const Question = styled.Text`
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin: 40px auto;
`;

export default ConfirmTeam;
