import React, {useEffect} from 'react';
import styled from 'styled-components/native';
import {StyleSheet, Text} from 'react-native';
import {MainH1, StyledBtn, BtnText} from '../../common/styled';
import Arrow from '../../assets/arrowLeft.svg';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import TagItem from './TagItem';
import {
  getAllInterests,
  updateEmployeeInterests,
} from '../../store/thunks/employeeThunks';

const styles = StyleSheet.create({
  flex5: {flex: 5},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
});

function Interests(props) {
  const dispatch = useDispatch();
  const {navigation} = props;
  const {allInterests, companyInterests} = useSelector(state => state.employee);

  const [arrInerests, setArrInterests] = useState([]);
  const [newInterestValue, setNewInterestValue] = useState('');

  useEffect(() => {
    dispatch(getAllInterests());
  }, [dispatch]);

  useEffect(() => {
    allInterests.length && setArrInterests(allInterests);
  }, [allInterests]);

  const handleAddNewInterest = () => {
    if (newInterestValue) {
      setArrInterests(prev => [
        ...prev,
        {
          text: newInterestValue,
          id: +(Math.random() * 10000).toFixed(),
          isChecked: true,
        },
      ]);
      setNewInterestValue('');
    }
  };

  const handleCheckInterest = id => {
    setArrInterests(prev => {
      let currentInterest;
      let currentIndex;
      let arr = prev.slice(0).filter((item, index) => {
        if (item.id === id) {
          currentInterest = item;
          currentIndex = index;
        }
        return item.id !== id;
      });
      currentInterest = {
        ...currentInterest,
        isChecked: !currentInterest.isChecked,
      };
      arr.splice(currentIndex, 0, currentInterest);
      return arr;
    });
  };

  const handleUpdateInterests = () => {
    navigation.navigate('YouReAllSet');
    dispatch(
      updateEmployeeInterests(
        arrInerests.reduce(
          (acc, cur) => {
            if (cur.isChecked) {
              if (companyInterests.includes(cur.text)) {
                acc.interests.push(cur.text);
              } else {
                acc.personalInterests.push(cur.text);
              }
            }
            return acc;
          },
          {
            interests: [],
            personalInterests: [],
          },
        ),
        // () => navigation.navigate('Friends'),
        () => {},
      ),
    );
  };

  return (
    <MainWrap>
      <Colored>
        <ColoredBackGround />
      </Colored>
      <MainScroll>
        <Head>
          <TopWrap>
            <ArrowWrap>
              <Arrow />
            </ArrowWrap>
            <MainH1 style={styles.flex5}>Tell Us About You</MainH1>
          </TopWrap>
          <StyledText>
            Let us know a bit about what you{'\n'} like so our algorithms can
            find the{'\n'} perfect place for you.
          </StyledText>
        </Head>
        <TagsWrap>
          {arrInerests.map(({text, id, isChecked}) => (
            <TagItem
              text={text}
              isChecked={isChecked}
              key={id}
              id={id}
              handleCheckInterest={handleCheckInterest}
            />
          ))}
        </TagsWrap>
        <AddWrap>
          <StyledINput
            onChangeText={setNewInterestValue}
            value={newInterestValue}
            placeholder="Add interest"
          />
          <AddBtn onPress={handleAddNewInterest}>
            <Text>Add</Text>
          </AddBtn>
        </AddWrap>
        <OnlyAndr />
      </MainScroll>
      <BtnsWrap style={styles.borderTop}>
        <StyledBtn color="#000000" onPress={handleUpdateInterests}>
          <BtnText color="#ffffff">OK</BtnText>
        </StyledBtn>
      </BtnsWrap>
    </MainWrap>
  );
}

const MainWrap = styled.SafeAreaView`
  flex: 1;
  position: relative;
`;
const StyledText = styled.Text`
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  color: #393a3d;
  margin: 30px 0 50px;
`;
const TopWrap = styled.View`
  padding: 0 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Head = styled.View`
  padding-top: 20px;
  width: 100%;
  background-color: #ffffff;
`;
const ArrowWrap = styled.TouchableOpacity`
  flex: 1;
  padding: 10px 20px;
  margin-bottom: 20px;
`;
const TagsWrap = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 25px 20px;
`;

const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #ffffff;
`;

const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const MainScroll = styled.ScrollView`
  width: 100%;
  flex: 1;
  background-color: #f5f5f5;
  padding-bottom: 80px;
`;
const AddWrap = styled.View`
  display: flex;
  flex-direction: row;
  padding: 0 20px;
`;
const BtnsWrap = styled.View`
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  justify-content: flex-end;
  width: 100%;
  padding: 16px 32px 0;
  background-color: #ffffff;
`;

const StyledINput = styled.TextInput`
  background-color: transparent;
  border: 1px solid #e8e8e8;
  border-radius: 8px;
  padding: 10px 16px;
  flex: 5;
  font-weight: 500;
  font-size: 16px;
`;

const AddBtn = styled.TouchableOpacity`
  padding: 7px 15px;
  border: 1px solid #000000;
  border-radius: 38px;
  margin-left: 15px;
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const OnlyAndr = styled.View`
  height: 125px;
`;

export default Interests;
