import React, {useState, useEffect} from 'react';
import styled from 'styled-components/native';
import {FlatList, ScrollView, StyleSheet} from 'react-native';
import _ from 'lodash';
import {useDispatch, useSelector} from 'react-redux';
import FriendsIcon from '../../assets/friendsIcon.svg';
import {
  getAllColleagues,
  getRelations,
  setRelations,
} from '../../store/thunks/colleaguesThunks';
import ColleagueItem from '../friendsAndCoworkers/ColleagueItem';
import FirstTimeUse from './FirstTimeUse';

const styles = StyleSheet.create({
  flex5: {flex: 8},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
});

const CoworkersBlock = props => {
  const {} = props;
  const dispatch = useDispatch();
  const {allColleagues, relations} = useSelector(state => state.colleagues);
  const {employeeData} = useSelector(state => state.employee);

  const [allColleaguesLocal, setAllColleaguesLocal] = useState([]);
  const [type, setType] = useState('today');
  const [searchValue, setSearchValue] = useState('');
  const [showOnly, setShowOnly] = useState('');

  const [todayInOffice, setTodayInOfiice] = useState([]);
  const [displayedList, setDisplayedList] = useState([]);

  useEffect(() => {
    if (employeeData.employeeId) {
      dispatch(getAllColleagues());
      dispatch(getRelations());
    }
  }, [employeeData, dispatch]);

  useEffect(() => {
    if (
      allColleagues.length &&
      employeeData.employeeId &&
      relations.colleagues
    ) {
      let arr = [];
      allColleagues.slice(0).forEach(item => {
        if (relations.colleagues?.find(col => col.employeeId === item.id)) {
          arr.push({
            ...item,
            isCoworker: true,
            isFriend: !!relations.friends?.find(
              col => col.employeeId === item.id,
            ),
          });
        } else if (relations.friends?.find(col => col.employeeId === item.id)) {
          arr.push({...item, isFriend: true});
        }
        if (employeeData.employeeId !== item.id) {
          arr.push(item);
        }
      });
      setAllColleaguesLocal(_.uniqBy(arr, 'id'));
    }
  }, [allColleagues, relations, employeeData]);

  useEffect(() => {
    if (!searchValue.trim() && !showOnly && relations.friends) {
      setDisplayedList(allColleaguesLocal);
      return;
    }
    let newList = allColleaguesLocal;
    if (searchValue.trim()) {
      newList = newList.filter(
        item =>
          item.firstName
            .toLowerCase()
            .includes(searchValue.trim().toLowerCase()) ||
          item.lastName
            .toLowerCase()
            .includes(searchValue.trim().toLowerCase()),
      );
    }
    if (showOnly) {
      newList = newList.filter(item => {
        if (showOnly === 'friend') {
          return item.isFriend;
        } else {
          return item.isCoworker;
        }
      });
    }
    setDisplayedList(newList);
  }, [searchValue, showOnly, allColleaguesLocal, relations]);

  const handleShowOnly = typee => {
    if (typee === 'friend') {
      if (!showOnly || showOnly === 'coworker') {
        setShowOnly('friend');
      } else {
        setShowOnly('');
      }
    } else if (typee === 'coworker') {
      if (!showOnly || showOnly === 'friend') {
        setShowOnly('coworker');
      } else {
        setShowOnly('');
      }
    }
  };

  const handleSubmitColleagues = () => {
    let friendIds = [];
    let colleagueIds = [];
    allColleaguesLocal.forEach(item => {
      if (item.isFriend) {
        friendIds.push(item.id);
      }
      if (item.isCoworker) {
        colleagueIds.push(item.id);
      }
    });
    dispatch(
      setRelations({
        companyId: relations.companyId,
        employeeId: relations.employeeId,
        friendIds: friendIds.length ? friendIds : null,
        colleagueIds: colleagueIds.length ? colleagueIds : null,
      }),
    );
  };

  return (
    <BlockWrap>
      <LogoContainer>
        <LogoWrap>
          <FriendsIcon />
        </LogoWrap>
      </LogoContainer>
      <Title>Friends & Coworkers</Title>
      <SwitchWrap>
        <WrapText active={type === 'today'} onPress={() => setType('today')}>
          <SwitchText active={type === 'today'}>Today</SwitchText>
        </WrapText>
        <WrapText active={type === 'change'} onPress={() => setType('change')}>
          <SwitchText active={type === 'change'}>
            Add or {'\n'}change
          </SwitchText>
        </WrapText>
        <WrapText active={type === 'next'} onPress={() => setType('next')}>
          <SwitchText active={type === 'next'}>For next {'\n'}week</SwitchText>
        </WrapText>
      </SwitchWrap>
      <DescriptionText>
        {type === 'today'
          ? 'See who’s planned to be in the office today.'
          : type === 'change'
          ? 'Add your colleagues below, so we can bring you in on the same days.'
          : 'Any specific requests for next week? Mark them in the list below so we can bring you in on the same days.'}
      </DescriptionText>
      <SearchField
        placeholder="Search for colleagues..."
        value={searchValue}
        onChangeText={setSearchValue}
      />
      {type === 'change' ? (
        <>
          <ShowSelectedWrap style={styles.borderBottom}>
            <TextName>Show selected:</TextName>
            <RadiosWrap>
              <StyledRadioBlack
                onPress={() => handleShowOnly('friend')}
                isChecked={showOnly === 'friend'}>
                <RadioText isChecked={showOnly === 'friend'}>Friend</RadioText>
              </StyledRadioBlack>
              <StyledRadioBlack
                onPress={() => handleShowOnly('coworker')}
                isChecked={showOnly === 'coworker'}>
                <RadioText isChecked={showOnly === 'coworker'}>
                  Coworker
                </RadioText>
              </StyledRadioBlack>
            </RadiosWrap>
          </ShowSelectedWrap>
          <ListContainer>
            <ScrollView nestedScrollEnabled>
              {displayedList.map(item => (
                <ColleagueItem
                  key={item.id}
                  colleague={item}
                  setDisplayedList={data => {
                    setAllColleaguesLocal(data);
                    setTimeout(handleSubmitColleagues, 2);
                  }}
                />
              ))}
            </ScrollView>
          </ListContainer>
        </>
      ) : type === 'next' ? (
        <FirstTimeUse colleagues={allColleagues} />
      ) : null}
    </BlockWrap>
  );
};

const BlockWrap = styled.View`
  width: 100%;
  padding: 18px;
  height: 580px;
  background-color: #f4f5f8;
  border-radius: 20px;
  margin: 50px 0;
`;

const LogoContainer = styled.View`
  align-self: center;
  position: relative;
  width: 48px;
  height: 48px;
`;

const LogoWrap = styled.View`
  position: absolute;
  top: -38px;
  width: 48px;
  height: 48px;
  background-color: #9ce9fa;
  border-radius: 48px;
  align-items: center;
  justify-content: center;
  padding-bottom: 3px;
  padding-right: 1px;
`;

const Title = styled.Text`
  margin-top: -30px;
  font-weight: bold;
  font-size: 22px;
  line-height: 34px;
  text-align: center;
  color: #000000;
`;

const SwitchWrap = styled.View`
  width: 310px;
  height: 38px;
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  padding: 1px;
  border-radius: 100px;
  align-self: center;
  margin: 15px 0;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;
const WrapText = styled.TouchableOpacity`
  width: 102px;
  height: 36px;
  background-color: ${props => (!props.active ? 'transparent' : '#fff')};
  border-radius: 100px;
  justify-content: center;
`;
const SwitchText = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  opacity: 0.55;
  text-align: center;
  color: ${props => (!props.active ? '#24262B' : '#00C0EB')};
`;

const DescriptionText = styled.Text`
  padding-left: 10px;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
`;

const SearchField = styled.TextInput`
  background-color: #fff;
  border: 1px solid #e8e8e8;
  border-radius: 8px;
  padding: 10px 15px;
  font-weight: 500;
  font-size: 16px;
  margin: 20px 10px;
`;

const ShowSelectedWrap = styled.View`
  padding-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const TextName = styled.Text`
  font-size: 16px;
  line-height: 22px;
  color: #000000;
`;

const RadiosWrap = styled.View`
  display: flex;
  flex-direction: row;
`;
const StyledRadioBlack = styled.TouchableOpacity`
  width: 70px;
  height: 24px;
  border: 1px solid #000000;
  border-radius: 50px;
  margin-left: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.isChecked ? '#000000' : 'transparent')};
`;

const RadioText = styled.Text`
  font-size: 12px;
  line-height: 22px;
  letter-spacing: -0.408px;
  color: ${props => (props.isChecked ? '#fff' : '#666666')};
`;

const ListContainer = styled.View`
  width: 100%;
  height: 250px;
`;
export default CoworkersBlock;
