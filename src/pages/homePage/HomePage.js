import React, {useEffect, useState, useRef} from 'react';
import styled from 'styled-components/native';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  PermissionsAndroid,
  Linking,
  Text,
} from 'react-native';
import WifiManager from 'react-native-wifi-reborn';
import Geolocation from 'react-native-geolocation-service';
import {useDispatch, useSelector} from 'react-redux';
import {getEmployeeData} from '../../store/thunks/employeeThunks';
import {MainH1} from '../../common/styled';
import Bell from '../../assets/bellIco.svg';
import CalendarIcon from '../../assets/calendarIco.svg';
import Test from '../../assets/test.svg';
import CoworkersBlock from './CoworkersBlock';
import CalendarComponent from '../../components/CalendarComponent';
import CalendarPopUps from '../../common/calendarPopUps';
import officeImg from '../../assets/office.png';
import {
  getTodayText,
  getGreetingByTime,
  requestLocationPermission,
} from '../../common/helpers';

const styles = StyleSheet.create({
  shadow: {
    width: 155,
    height: 155,
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 155,
    overflow: 'hidden',
    marginBottom: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  image: {
    // zIndex: 200,
    width: 152,
    height: 152,
  },
  blurWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  blur: {
    zIndex: 10,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  marginH: {
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 40,
    textAlign: 'center',
  },
  centered: {
    marginTop: 10,
    marginBottom: 25,
    alignItems: 'center',
  },
  textA: {
    textAlign: 'center',
    maxWidth: 300,
  },
  centeredT: {
    textAlign: 'center',
    maxWidth: 300,
    lineHeight: 32,
  },
  arrived: {
    position: 'absolute',
    top: 3,
    left: 2,
  },
});

const HomePage = props => {
  const dispatch = useDispatch();
  const scrollViewRef = useRef();
  const {employeeData} = useSelector(state => state.employee);
  const {todayAnchor} = useSelector(state => state.anchors);
  const [arrived, setArrived] = useState(false);
  const [arrivingByCar, setArrivingByCar] = useState(false);
  const [dayTimeText, setDayTimeText] = useState('');

  const [wifi, setWifi] = useState('');
  const [location, setLocation] = useState(null);

  const testik = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Location permission is required for WiFi connections',
        message:
          'This app needs location permission as this is required  ' +
          'to scan for wifi networks.',
        buttonNegative: 'DENY',
        buttonPositive: 'ALLOW',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      WifiManager.getCurrentWifiSSID().then(
        ssid => {
          console.log('Your current connected wifi SSID is ' + ssid);
          if (ssid) {
            setWifi(ssid);
          }
        },
        () => {},
      );
      let locationData = await new Promise((resolve, reject) =>
        Geolocation.getCurrentPosition(resolve, reject, {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
        }),
      );
      if (locationData) {
        setLocation({
          lon: locationData.coords.longitude,
          lat: locationData.coords.latitude,
        });
      }
      console.log('.coords.latitude,', locationData?.coords.latitude);
      console.log('.coords.longitude,', locationData?.coords.longitude);
    } else {
    }
  };

  useEffect(() => {
    dispatch(getEmployeeData({}));
    testik();
    setDayTimeText(getGreetingByTime());
  }, [dispatch]);

  return (
    <>
      <MainWrap>
        <Colored>
          <ColoredBackGround />
          <WhiteBackGround />
        </Colored>
        <MainScroll ref={scrollViewRef}>
          <Content>
            <TopIconsWrap>
              <BellWrap>
                <Bell />
                <RedBellDot>
                  <DotText>2</DotText>
                </RedBellDot>
              </BellWrap>
              <TouchableOpacity
                onPress={() => {
                  console.log('scrollViewRef.current', scrollViewRef.current);
                  scrollViewRef.current.scrollToEnd({animated: true});
                }}>
                <CalendarIcon />
              </TouchableOpacity>
            </TopIconsWrap>
            <MainH1 style={styles.textA}>
              {dayTimeText} {employeeData.employeeName?.split(' ')[0]}!
            </MainH1>
            <View style={styles.shadow}>
              <Image style={styles.image} source={officeImg} />
            </View>
            <MainH1 style={styles.centeredT}>
              {todayAnchor ? getTodayText(todayAnchor) : ''}
            </MainH1>
            <ArraivedWrap>
              <ArrivedBtn
                arrived={arrivingByCar}
                onPress={() => setArrivingByCar(prev => !prev)}>
                {arrivingByCar ? <Test style={styles.arrived} /> : null}
                <ArrivedBtnText arrived={arrivingByCar}>
                  Arriving by car
                </ArrivedBtnText>
              </ArrivedBtn>
              <ArrivedBtn
                arrived={arrived}
                onPress={() => setArrived(prev => !prev)}>
                {arrived ? <Test style={styles.arrived} /> : null}
                <ArrivedBtnText arrived={arrived}>
                  I’ve arrived! 😎
                </ArrivedBtnText>
              </ArrivedBtn>
            </ArraivedWrap>
            <CoworkersBlock />
            <CalendarBlockWrap>
              <LogoContainer>
                <LogoWrap>
                  <CalendarIcon />
                </LogoWrap>
              </LogoContainer>
              <CalendarComponent />
            </CalendarBlockWrap>
            <TouchableOpacity
              onPress={() =>
                Linking.openURL('https://www.exybrid.com/privacy-policy')
              }>
              <PolicyLink>Privacy policy</PolicyLink>
            </TouchableOpacity>
            <View style={{minHeight: 60}}>
              {wifi ? (
                <Text>Your current connected wifi SSID is {wifi}</Text>
              ) : null}
              {location ? (
                <>
                  <Text>lon: {location.lon}</Text>
                  <Text>lat: {location.lat}</Text>
                </>
              ) : null}
            </View>
          </Content>
        </MainScroll>
      </MainWrap>
      <CalendarPopUps />
    </>
  );
};

const MainWrap = styled.SafeAreaView`
  flex: 1;
`;
const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #96e1f2;
`;
const WhiteBackGround = styled.View`
  flex: 5;
  background-color: #ffffff;
`;
const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const MainScroll = styled.ScrollView`
  /* width: 100%;
  flex: 1; */
`;
const Content = styled.View`
  margin: 20px 24px 0;
  flex: 1;
  align-items: center;
`;

const TopIconsWrap = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;
const BellWrap = styled.TouchableOpacity`
  position: relative;
`;
const RedBellDot = styled.View`
  position: absolute;
  top: -1px;
  right: -4px;
  width: 14px;
  height: 14px;
  border-radius: 14px;
  background-color: #ff0000;
  justify-content: center;
`;
const DotText = styled.Text`
  color: #fff;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
`;

const ArraivedWrap = styled.View`
  width: 100%;
  max-width: 400px;
  flex-direction: row;
  justify-content: space-between;
`;
const ArrivedBtn = styled.TouchableOpacity`
  position: relative;
  border: ${props =>
    props.arrived ? '1px solid #2CA01C' : '1px solid #000000'};
  padding-left: ${props => (props.arrived ? '37px' : '30px')};
  padding-right: ${props => (props.arrived ? '23px' : '30px')};
  height: 30px;
  border-radius: 60px;
  justify-content: center;
`;
const ArrivedBtnText = styled.Text`
  font-size: 14px;
  text-align: center;
  color: ${props => (props.arrived ? '#666666' : '#000000')};
`;
const CalendarBlockWrap = styled.View`
  width: 100%;
  padding-top: 18px;
  background-color: #f4f5f8;
  border-radius: 20px;
  margin-bottom: 25px;
`;
const LogoContainer = styled.View`
  align-self: center;
  position: relative;
  width: 48px;
  height: 48px;
`;

const LogoWrap = styled.View`
  position: absolute;
  top: -38px;
  width: 48px;
  height: 48px;
  background-color: #9ce9fa;
  border-radius: 48px;
  align-items: center;
  justify-content: center;
  padding-bottom: 3px;
  padding-right: 1px;
`;
const PolicyLink = styled.Text`
  text-align: center;
  font-size: 14px;
  line-height: 24px;
  color: #8f8f8f;
  margin-bottom: 25px;
`;
export default HomePage;
