import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import _ from 'lodash';
import {MainH1, StyledBtn, BtnText, Empty} from '../../common/styled';
import Arrow from '../../assets/arrowLeft.svg';
import {useDispatch, useSelector} from 'react-redux';
import {
  getAllColleagues,
  getRelations,
  setRelations,
} from '../../store/thunks/colleaguesThunks';
import ColleagueItem from './ColleagueItem';

const styles = StyleSheet.create({
  flex5: {flex: 8},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
});

function FriendsCoworkers({navigation}) {
  const dispatch = useDispatch();
  const {allColleagues, relations} = useSelector(state => state.colleagues);
  const {employeeData} = useSelector(state => state.employee);

  const [allColleaguesLocal, setAllColleaguesLocal] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [showOnly, setShowOnly] = useState('');
  const [displayedList, setDisplayedList] = useState([]);

  useEffect(() => {
    dispatch(getAllColleagues());
    dispatch(getRelations());
  }, [dispatch]);

  useEffect(() => {
    if (
      allColleagues.length &&
      employeeData.employeeId &&
      relations.colleagues
    ) {
      let arr = [];
      allColleagues.slice(0).forEach(item => {
        if (relations.colleagues?.find(col => col.employeeId === item.id)) {
          arr.push({
            ...item,
            isCoworker: true,
            isFriend: !!relations.friends?.find(
              col => col.employeeId === item.id,
            ),
          });
        } else if (relations.friends?.find(col => col.employeeId === item.id)) {
          arr.push({...item, isFriend: true});
        }
        if (employeeData.employeeId !== item.id) {
          arr.push(item);
        }
      });
      setAllColleaguesLocal(_.uniqBy(arr, 'id'));
    }
  }, [allColleagues, relations, employeeData]);

  useEffect(() => {
    if (!searchValue.trim() && !showOnly && relations.friends) {
      setDisplayedList(allColleaguesLocal);
      return;
    }
    let newList = allColleaguesLocal;
    if (searchValue.trim()) {
      newList = newList.filter(
        item =>
          item.firstName
            .toLowerCase()
            .includes(searchValue.trim().toLowerCase()) ||
          item.lastName
            .toLowerCase()
            .includes(searchValue.trim().toLowerCase()),
      );
    }
    if (showOnly) {
      newList = newList.filter(item => {
        if (showOnly === 'friend') {
          return item.isFriend;
        } else {
          return item.isCoworker;
        }
      });
    }
    setDisplayedList(newList);
  }, [searchValue, showOnly, allColleaguesLocal, relations]);

  const handleShowOnly = type => {
    if (type === 'friend') {
      if (!showOnly || showOnly === 'coworker') {
        setShowOnly('friend');
      } else {
        setShowOnly('');
      }
    } else if (type === 'coworker') {
      if (!showOnly || showOnly === 'friend') {
        setShowOnly('coworker');
      } else {
        setShowOnly('');
      }
    }
  };

  const handleSubmitColleagues = () => {
    let friendIds = [];
    let colleagueIds = [];

    allColleaguesLocal.forEach(item => {
      if (item.isFriend) {
        friendIds.push(item.id);
      }
      if (item.isCoworker) {
        colleagueIds.push(item.id);
      }
    });

    dispatch(
      setRelations({
        companyId: relations.companyId,
        employeeId: relations.employeeId,
        friendIds: friendIds.length ? friendIds : null,
        colleagueIds: colleagueIds.length ? colleagueIds : null,
      }),
    );
    navigation.navigate('Anchors');
  };

  return (
    <MainWrap>
      <Colored>
        <ColoredBackGround />
      </Colored>
      <MainScroll>
        <Head>
          <TopWrap>
            <ArrowWrap>
              <Arrow />
            </ArrowWrap>
            <MainH1 style={styles.flex5}>Add Friends and Coworkers</MainH1>
          </TopWrap>
          <StyledText>
            Make your days fun and impactful - select {'\n'}your colleagues
            below so we can bring you {'\n'}in on the same days. same days.
          </StyledText>
        </Head>
        <SearchField
          placeholder="Search for colleagues..."
          value={searchValue}
          onChangeText={setSearchValue}
        />
        <ShowSelectedWrap style={styles.borderBottom}>
          <TextName>Show selected:</TextName>
          <RadiosWrap>
            <StyledRadioBlack
              onPress={() => handleShowOnly('friend')}
              isChecked={showOnly === 'friend'}>
              <RadioText isChecked={showOnly === 'friend'}>Friend</RadioText>
            </StyledRadioBlack>
            <StyledRadioBlack
              onPress={() => handleShowOnly('coworker')}
              isChecked={showOnly === 'coworker'}>
              <RadioText isChecked={showOnly === 'coworker'}>
                Coworker
              </RadioText>
            </StyledRadioBlack>
          </RadiosWrap>
        </ShowSelectedWrap>
        <ListContainer>
          {displayedList.map(item => (
            <ColleagueItem
              key={item.id}
              colleague={item}
              setDisplayedList={setAllColleaguesLocal}
            />
          ))}
        </ListContainer>
        <Empty />
      </MainScroll>
      <BtnsWrap style={styles.borderTop}>
        <StyledBtn color="#000000" onPress={handleSubmitColleagues}>
          <BtnText color="#ffffff">OK</BtnText>
        </StyledBtn>
      </BtnsWrap>
    </MainWrap>
  );
}

const MainWrap = styled.SafeAreaView`
  flex: 1;
  position: relative;
`;
const StyledText = styled.Text`
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  color: #393a3d;
  margin: 20px 0 20px;
`;
const TopWrap = styled.View`
  padding: 0 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Head = styled.View`
  padding-top: 20px;
  width: 100%;
  background-color: #ffffff;
`;
const ArrowWrap = styled.TouchableOpacity`
  flex: 1;
  padding: 10px 0;
  margin-bottom: 20px;
`;
const ColoredBackGround = styled.View`
  flex: 2;
  background-color: #ffffff;
`;

const Colored = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
const MainScroll = styled.ScrollView`
  width: 100%;
  flex: 1;
  background-color: #ffffff;
  padding-bottom: 80px;
`;
const BtnsWrap = styled.View`
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  justify-content: flex-end;
  width: 100%;
  padding: 16px 32px 0;
  background-color: #ffffff;
`;

const SearchField = styled.TextInput`
  background-color: #f6f6f6;
  border: 1px solid #e8e8e8;
  border-radius: 8px;
  padding: 15px;
  font-weight: 500;
  font-size: 16px;
  margin: 20px;
`;

const ShowSelectedWrap = styled.View`
  padding: 0 20px 20px 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const TextName = styled.Text`
  font-size: 16px;
  line-height: 22px;
  color: #000000;
`;

const RadiosWrap = styled.View`
  display: flex;
  flex-direction: row;
`;
const StyledRadioBlack = styled.TouchableOpacity`
  width: 70px;
  height: 24px;
  border: 1px solid #000000;
  border-radius: 50px;
  margin-left: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.isChecked ? '#000000' : 'transparent')};
`;

const RadioText = styled.Text`
  font-size: 12px;
  line-height: 22px;
  letter-spacing: -0.408px;
  color: ${props => (props.isChecked ? '#fff' : '#666666')};
`;

const ListContainer = styled.View`
  padding: 0 20px 0 15px;
`;

export default FriendsCoworkers;
