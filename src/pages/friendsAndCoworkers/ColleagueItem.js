import React from 'react';
import styled from 'styled-components/native';
import {StyleSheet, View, Image} from 'react-native';
import User from '../../assets/default-user.png';

const styles = StyleSheet.create({
  flex5: {flex: 5},
  borderTop: {
    borderTopWidth: 1,
    borderColor: '#BDC5CD',
  },
  borderBottom: {
    paddingBottom: 14,
    borderBottomWidth: 1,
    borderColor: '#bdc5cd',
  },
  shadow: {
    width: 29,
    height: 29,
    borderWidth: 1,
    borderColor: '#DADADA',
    borderRadius: 29,
    overflow: 'hidden',
    marginRight: 8,
  },
  image: {
    width: 29,
    height: 29,
  },
});
function TextAbstract(text, length) {
  if (text == null) {
    return '';
  }
  if (text.length <= length) {
    return text;
  }
  text = text.substring(0, length);
  let last = text.lastIndexOf(' ');
  text = text.substring(0, last);
  return text + '...';
}

function ColleagueItem({colleague, setDisplayedList}) {
  const handleClickColleague = type => {
    setDisplayedList(prev => {
      return prev.slice(0).map(item => {
        if (item.id === colleague.id) {
          if (type === 'friend') {
            if (!item.isFriend) {
              item.isFriend = true;
            } else {
              item.isFriend = false;
            }
          } else if (type === 'coworker') {
            if (!item.isCoworker) {
              item.isCoworker = true;
            } else {
              item.isCoworker = false;
            }
          }
        }
        return item;
      });
    });
  };

  return (
    <ItemWrap style={styles.borderBottom}>
      <View style={styles.shadow}>
        <Image
          style={styles.image}
          source={colleague.image ? {uri: colleague.image} : User}
        />
      </View>
      <StyledName>
        {TextAbstract(`${colleague.firstName} ${colleague.lastName}`, 20)}
      </StyledName>
      <RadiosWrap>
        <StyledRadioBlue
          onPress={() => handleClickColleague('friend')}
          isChecked={colleague.isFriend}>
          <RadioText isChecked={colleague.isFriend}>Friend</RadioText>
        </StyledRadioBlue>
        <StyledRadioBlue
          onPress={() => handleClickColleague('coworker')}
          isChecked={colleague.isCoworker}>
          <RadioText isChecked={colleague.isCoworker}>Coworker</RadioText>
        </StyledRadioBlue>
      </RadiosWrap>
    </ItemWrap>
  );
}

const ItemWrap = styled.SafeAreaView`
  flex: 1;
  height: 49px;
  padding: 14px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const StyledName = styled.Text`
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.408px;
  color: #000000;
  margin-right: auto;
`;
const RadiosWrap = styled.View`
  display: flex;
  flex-direction: row;
`;
const StyledRadioBlue = styled.TouchableOpacity`
  width: 70px;
  height: 24px;
  border: 1px solid #00c0eb;
  border-radius: 50px;
  margin-left: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.isChecked ? '#00c0eb' : 'transparent')};
`;

const RadioText = styled.Text`
  font-size: 12px;
  line-height: 22px;
  letter-spacing: -0.408px;
  color: ${props => (props.isChecked ? '#fff' : '#666666')};
`;

export default ColleagueItem;
