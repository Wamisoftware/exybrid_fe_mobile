import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector, useDispatch} from 'react-redux';
import {getItem} from './api/asyncStorage';
import {setAccountToken} from './store/actions/accountActions';
import LoginPage from './pages/Login';
import ConfirmTeam from './pages/ConfirmTeam';
import Interests from './pages/interests/Interests';
import EnterCode from './pages/EnterCode';
import FriendsCoworkers from './pages/friendsAndCoworkers/FriendsCoworkers';
import Anchors from './pages/anchors/Anchors';
import HomePage from './pages/homePage/HomePage';
import YouReAllSet from './pages/YouReAllSet';

const Stack = createStackNavigator();

function Main() {
  const dispatch = useDispatch();
  const token = useSelector(state => state.account.token);
  const [isSigned, setIsSigned] = useState(false);

  useEffect(() => {
    const checkToken = async () => {
      const storToken = await getItem('tokens');
      if (token || storToken) {
        if (!token && storToken) {
          dispatch(setAccountToken(storToken));
        }
        setIsSigned(true);
      } else {
        setIsSigned(false);
      }
    };
    checkToken();
  }, [token, dispatch]);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="HomePage" component={HomePage} />
        <Stack.Screen name="Login" component={LoginPage} />
        <Stack.Screen name="ConfirmTeam" component={ConfirmTeam} />
        <Stack.Screen name="Friends" component={FriendsCoworkers} />
        <Stack.Screen name="Enter code" component={EnterCode} />
        <Stack.Screen name="Anchors" component={Anchors} />
        <Stack.Screen name="Interests" component={Interests} />
        <Stack.Screen name="YouReAllSet" component={YouReAllSet} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Main;
