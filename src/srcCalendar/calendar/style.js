import {StyleSheet} from 'react-native';
import * as defaultStyle from '../style';

const STYLESHEET_ID = 'stylesheet.calendar.main';

export default function getStyle(theme = {}) {
  const appStyle = {...defaultStyle, ...theme};
  return StyleSheet.create({
    container: {
      paddingLeft: 10,
      marginRight: -125,
      // backgroundColor: appStyle.calendarBackground,
      backgroundColor: 'transparent',
    },
    dayContainer: {
      flex: 1,
      alignItems: 'center',
    },
    emptyDayContainer: {
      flex: 1,
    },
    monthView: {
      width: '100%',
      // backgroundColor: appStyle.calendarBackground,
      backgroundColor: 'transparent',
    },
    week: {
      marginTop: 7,
      marginBottom: 7,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    ...(theme[STYLESHEET_ID] || {}),
  });
}
