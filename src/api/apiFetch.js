import {API_URL} from './constants';

export const apiFetch = async data => {
  const {url, headers, method, body, dispatch, cb, loading = true} = data;

  const options = {
    method,
    body,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers,
    },
  };
  try {
    const response = await fetch(API_URL + url, options);
    // console.log('response', response);
    if (response.status === 200) {
      if (url.includes('authenticate')) {
        return {data: await response.text()};
      } else {
        return {data: await response.json()};
      }
    } else {
      return {error: response};
    }
  } catch (error) {
    console.log('error from catch', error);
    return {error};
  }
};
