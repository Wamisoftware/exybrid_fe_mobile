import React from 'react';
import {useSelector} from 'react-redux';
import {View, StyleSheet, ActivityIndicator} from 'react-native';

const styles = StyleSheet.create({
  loadingWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  spinnerWrap: {
    zIndex: 40,
    width: '100%',
    height: '100%',
    // backgroundColor: 'rgba(247, 247, 247, 0.9)',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Loading = () => {
  const isLoading = useSelector(state => state.loading);

  return (
    isLoading && (
      <View style={styles.loadingWrap}>
        <View style={styles.spinnerWrap}>
          <ActivityIndicator size="large" color="#61933A" />
        </View>
      </View>
    )
  );
};

export default Loading;
