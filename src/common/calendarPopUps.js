import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  setAddPopup,
  setAllAnchors,
  setChangePopup,
  setExistingPopup,
} from '../store/actions/calendarPopupAction';
import AddAnchorPopup from '../pages/anchors/popups/AddAnchorPopup';
import ChangeAnchorPopup from '../pages/anchors/popups/ChangeAnchorPopup';
import ExistingAnchorPopup from '../pages/anchors/popups/ExistingAnchorsPopup';

const CalendarPopUps = () => {
  const dispatch = useDispatch();
  const {addPopup, changePopup, existingPopup, dataToPopup} = useSelector(
    state => state.calendarPopUp,
  );

  const handleSet = payload => dispatch(setAllAnchors(payload));

  return (
    <>
      {addPopup && (
        <AddAnchorPopup
          data={dataToPopup}
          close={() => dispatch(setAddPopup(false))}
          setAllAnchors={handleSet}
        />
      )}
      {changePopup && (
        <ChangeAnchorPopup
          data={dataToPopup}
          close={() => dispatch(setChangePopup(false))}
          setAllAnchors={handleSet}
        />
      )}
      {existingPopup && (
        <ExistingAnchorPopup
          data={dataToPopup}
          close={() => dispatch(setExistingPopup(false))}
          setAllAnchors={handleSet}
        />
      )}
    </>
  );
};

export default CalendarPopUps;
