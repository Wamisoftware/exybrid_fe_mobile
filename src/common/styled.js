import styled from 'styled-components/native';

export const MainH1 = styled.Text`
  font-size: 24px;
  font-weight: bold;
  line-height: 28px;
  color: #000000;
  margin-bottom: 20px;
`;

export const StyledBtn = styled.TouchableOpacity`
  padding: 16px 32px;
  background-color: ${props => props.color};
  border: ${props =>
    props.border ? '1px solid #000000' : `1px solid ${props.color}`};
  border-radius: 60px;
  align-items: center;
  margin-bottom: 20px;
  min-height: 50px;
`;

export const BtnText = styled.Text`
  color: ${props => props.color};
  font-weight: 600;
  font-size: 16px;
`;

export const NormalText = styled.Text`
  font-size: 16px;
  line-height: 19px;
  color: #000000;
`;

export const NormalGrayText = styled.Text`
  font-size: 16px;
  line-height: 19px;
  color: #6b6c72;
`;

export const GrayText = styled.Text`
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #666666;
`;

export const Empty = styled.View`
  height: 125px;
`;

export const PopupText = styled.Text`
  width: 100%;
  font-weight: 500;
  font-size: 16px;
  line-height: 21px;
  color: #24262b;
`;

export const DropText = styled.Text`
  /* width: 100%; */
  font-weight: 500;
  font-size: 16px;
  line-height: 21px;
  color: #0077c5;
`;
