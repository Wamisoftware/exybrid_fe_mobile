import {PermissionsAndroid} from 'react-native';

export const getDateStr = date =>
  `${date.getFullYear()}-${
    date.getMonth().toString().length === 1
      ? '0' + (date.getMonth() + 1)
      : date.getMonth() + 1
  }-${
    date.getDate().toString().length === 1
      ? '0' + date.getDate()
      : date.getDate()
  }`;

export const getTodayText = data => {
  const type = data?.anchorDayType;
  return type === 'office'
    ? 'Today you are planned to work from the office.'
    : type === 'home'
    ? 'Today you are planned to work from home.'
    : type === 'travel'
    ? 'Safe travels.'
    : 'Today you are out of office.';
};

export const getGreetingByTime = () => {
  const hours = new Date().getHours();

  if (hours >= 5 && hours < 12) {
    return 'Good Morning';
  } else if (hours >= 12 && hours < 17) {
    return 'Good afternoon';
  } else if (hours >= 17 && hours < 20) {
    return 'Good evening';
  } else if (hours >= 20 && hours < 5) {
    return 'Good nigth';
  } else {
    return 'hello';
  }
};

export const requestLocationPermission = async () => {
  try {
    const permissions = await PermissionsAndroid.requestMultiple(
      [
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ],
      {
        title: 'GPS Location Permission',
        message: 'App needs access to your GPS geolocation ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (
      permissions.ACCESS_FINE_LOCATION === PermissionsAndroid.RESULTS.GRANTED &&
      permissions.ACCESS_COARSE_LOCATION === PermissionsAndroid.RESULTS.GRANTED
    ) {
      console.log('You can use the geolocation');
    } else {
      console.log('Geolocation denied');
    }
  } catch (err) {
    console.warn(err);
  }
};
