import * as types from '../actionTypes';

const initialState = {
  error: null,
  isLoading: false,
  token: null,
  email: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case types.GET_CODE_REQUEST:
      return {...state, email: null};
    case types.GET_CODE_SUCCESS:
      return {...state, email: payload};
    case types.GET_CODE_FAIL:
      return {...state, error: payload};
    case types.SET_ACCOUNT_TOKEN:
      return {
        ...state,
        token: payload,
      };
    case types.USER_LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case types.USER_LOGIN_SUCCESS:
      return {...state, token: payload, isLoading: false};
    case types.USER_LOGIN_FAIL:
      return {...state, isLoading: false, error: payload};
    case types.USER_LOGOUT_ACCOUNT:
      return initialState;

    default:
      return state;
  }
};
