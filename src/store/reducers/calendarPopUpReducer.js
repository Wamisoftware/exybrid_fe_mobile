import * as types from '../actionTypes';

const initialState = {
  addPopup: false,
  changePopup: false,
  existingPopup: false,
  dataToPopup: {},
  allAnchors: [],
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case types.SET_ADD_POPUP:
      return {...state, addPopup: payload};
    case types.SET_CHANGE_POPUP:
      return {...state, changePopup: payload};
    case types.SET_EXISTING_POPUP:
      return {...state, existingPopup: payload};
    case types.SET_DATA_TO_POPUP:
      return {...state, dataToPopup: payload};
    case types.SET_ALL_ANCHORS:
      return {...state, allAnchors: payload};
    default:
      return state;
  }
};
