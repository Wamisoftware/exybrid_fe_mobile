import * as types from '../actionTypes';

const initialState = {
  isLoadingAllColleagues: false,
  isLoadingRelations: false,
  error: null,
  allColleagues: [],
  relations: {},
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case types.GET_ALL_COLLEAGUES_REQUEST:
      return {...state, isLoadingAllColleagues: true, allColleagues: []};
    case types.GET_ALL_COLLEAGUES_SUCCESS:
      return {...state, allColleagues: payload, isLoadingAllColleagues: false};
    case types.GET_EMPLOYEE_FAIL:
      return {...state, isLoadingAllColleagues: false, error: payload};
    case types.GET_RELATIONS_REQUEST:
      return {...state, isLoadingRelations: true, relations: {}};
    case types.GET_RELATIONS_SUCCESS:
      return {...state, isLoadingRelations: false, relations: payload};
    case types.GET_RELATIONS_FAIL:
      return {...state, isLoadingRelations: false, error: payload};
    case types.SET_RELATIONS_REQUEST:
      return {...state};
    case types.SET_RELATIONS_SUCCESS:
      console.log('SET_RELATIONS_SUCCESS', payload);
      return {...state};
    case types.SET_RELATIONS_FAIL:
      return {...state};
    default:
      return state;
  }
};
