import * as types from '../actionTypes';

export default (state = false, {type}) => {
  switch (type) {
    case types.SHOW_LOADING:
      return true;
    case types.REMOVE_LOADING:
      return false;
    default:
      return state;
  }
};
