import {getDateStr} from '../../common/helpers';
import * as types from '../actionTypes';

const initialState = {
  isLoadingAnchors: false,
  isLoadingReasons: false,
  anchorsList: [],
  error: null,
  exceptionReasons: [],
  removableAnchor: null,
  todayAnchor: null,
};

export default (state = initialState, {type, payload}) => {
  const todayDateStr = getDateStr(new Date());
  switch (type) {
    case types.GET_ALL_ANCHORS_REQUEST:
      return {...state, isLoadingAnchors: true, anchorsList: []};
    case types.GET_ALL_ANCHORS_SUCCESS:
      const anhor = payload.find(item => item.date === todayDateStr);
      return {
        ...state,
        isLoadingAnchors: false,
        anchorsList: payload,
        todayAnchor: anhor || {},
      };
    case types.GET_ALL_ANCHORS_FAIL:
      return {...state, isLoadingAnchors: false, error: payload};
    case types.ADD_ANCHOR_REQUEST:
      return {...state};
    case types.ADD_ANCHOR_SUCCESS:
      console.log('ADD_ANCHOR_SUCCESS', payload);
      const anhor1 = payload.find(item => item.date === todayDateStr);
      return {
        ...state,
        anchorsList: [...state.anchorsList, payload[0]],
        todayAnchor: anhor1 || {},
      };
    case types.ADD_ANCHOR_FAIL:
      return {...state, error: payload};
    case types.CHANGE_ANCHOR_REQUEST:
      return {...state};
    case types.CHANGE_ANCHOR_SUCCESS:
      console.log('CHANGE_ANCHOR_SUCCESS', payload);
      const anhor2 = payload.find(item => item.date === todayDateStr);
      const changedList = state.anchorsList.slice(0).map(item => {
        if (
          item.date === payload[0].date &&
          item.anchorType === payload[0].anchorType
        ) {
          return payload[0];
        } else {
          return item;
        }
      });
      return {...state, anchorsList: changedList, todayAnchor: anhor2 || {}};
    case types.CHANGE_ANCHOR_FAIL:
      return {...state, error: payload};
    case types.DELETE_ANCHOR_REQUEST:
      return {...state, removableAnchor: payload};
    case types.DELETE_ANCHOR_SUCCESS:
      let listAfterDeleting = state.anchorsList
        .slice(0)
        .filter(
          item =>
            item.date !== state.removableAnchor.date &&
            item.anchorType !== state.removableAnchor.anchorType,
        );
      const anhor3 = listAfterDeleting.find(item => item.date === todayDateStr);
      return {
        ...state,
        anchorsList: payload ? listAfterDeleting : state.anchorsList,
        todayAnchor: anhor3 || {},
      };
    case types.DELETE_ANCHOR_FAIL:
      return {...state, error: payload};
    case types.REQUEST_ANCHOR_EXCEPTION_REQUEST:
      return {...state};
    case types.REQUEST_ANCHOR_EXCEPTION_SUCCESS:
      console.log('REQUEST_ANCHOR_EXCEPTION_SUCCESS', payload);
      const listWithRequest = state.anchorsList.slice(0).map(item => {
        if (
          item.date === payload.date &&
          item.anchorType === payload.anchorType
        ) {
          return payload;
        } else {
          return item;
        }
      });
      return {...state, anchorsList: listWithRequest};
    case types.REQUEST_ANCHOR_EXCEPTION_FAIL:
      return {...state, error: payload};
    case types.CANCEL_REQUEST_EXCEPTION_REQUEST:
      return {...state};
    case types.CANCEL_REQUEST_EXCEPTION_SUCCESS:
      console.log('CANCEL_REQUEST_EXCEPTION_SUCCESS', payload);
      const listWithoutRequest = state.anchorsList.slice(0).map(item => {
        if (
          item.date === payload.date &&
          item.anchorType === payload.anchorType
        ) {
          return payload;
        } else {
          return item;
        }
      });
      return {...state, anchorsList: listWithoutRequest};
    case types.CANCEL_REQUEST_EXCEPTION_FAIL:
      return {...state, error: payload};
    case types.GET_EXCEPTION_REASONS_REQUEST:
      return {...state, isLoadingReasons: true, exceptionReasons: []};
    case types.GET_EXCEPTION_REASONS_SUCCESS:
      return {...state, isLoadingReasons: false, exceptionReasons: payload};
    case types.GET_EXCEPTION_REASONS_FAIL:
      return {...state, isLoadingReasons: false, error: payload};
    default:
      return state;
  }
};
