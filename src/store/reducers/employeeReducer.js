import * as types from '../actionTypes';

const initialState = {
  error: null,
  isLoading: false,
  employeeData: {},
  allInterests: [],
  companyInterests: [],
  employeeInterests: {},
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case types.GET_EMPLOYEE_REQUEST:
      return {
        ...state,
        isLoading: true,
        employeeData: {},
      };
    case types.GET_EMPLOYEE_SUCCESS:
      return {...state, employeeData: payload, isLoading: false};
    case types.GET_EMPLOYEE_FAIL:
      return {...state, isLoading: false, error: payload};
    case types.GET_ALL_INTERESTS_REQUEST:
      return {
        ...state,
        isLoading: true,
        allInterests: [],
        companyInterests: [],
        employeeInterests: {},
      };
    case types.GET_ALL_INTERESTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        allInterests: payload.all,
        companyInterests: payload.сompanyInterests,
        employeeInterests: payload.employeeInterests,
      };
    case types.GET_ALL_INTERESTS_FAIL:
      return {...state, isLoading: false, error: payload};
    default:
      return state;
  }
};
