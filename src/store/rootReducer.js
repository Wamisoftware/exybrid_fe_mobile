import {combineReducers} from 'redux';
import employeeReducer from './reducers/employeeReducer';
import loadingReducer from './reducers/loadingReducer';
import colleaguesReducer from './reducers/colleaguesReducer';
import anchorsReducer from './reducers/anchorsReducer';
import accountReducer from './reducers/accountReducer';
import calendarPopUpReducer from './reducers/calendarPopUpReducer';

const rootReducer = combineReducers({
  account: accountReducer,
  loading: loadingReducer,
  employee: employeeReducer,
  colleagues: colleaguesReducer,
  anchors: anchorsReducer,
  calendarPopUp: calendarPopUpReducer,
});

export default rootReducer;
