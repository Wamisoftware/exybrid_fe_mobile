import {apiFetch} from '../../api/apiFetch';
import {
  addAnchorFail,
  addAnchorRequest,
  addAnchorSuccess,
  cancelRequestExceptionFail,
  cancelRequestExceptionRequest,
  cancelRequestExceptionSuccess,
  changeAnchorFail,
  changeAnchorRequest,
  changeAnchorSuccess,
  deleteAnchorFail,
  deleteAnchorRequest,
  deleteAnchorSuccess,
  getAnchorsFail,
  getAnchorsRequest,
  getAnchorsSuccess,
  getExceptionReasonsFail,
  getExceptionReasonsRequest,
  getExceptionReasonsSuccess,
  requestAnchorExceptionFail,
  requestAnchorExceptionRequest,
  requestAnchorExceptionSuccess,
} from '../actions/anchorsAction';
import {removeLoading, showLoading} from '../actions/loadingActions';
import {store} from '../store';
import {getToken} from '../../api/asyncStorage';
import {getDateStr} from '../../common/helpers';

export const getAllAnchors =
  ({startDate = getDateStr(new Date()), endDate = '2021-06-30'}) =>
  async dispatch => {
    dispatch(showLoading());
    dispatch(getAnchorsRequest());
    const {employeeData} = store.getState().employee;
    const token = await getToken();
    const employeeId = employeeData.employeeId;
    const teamId = employeeData.teamId;
    const managedTeamId = employeeData.managedTeamId;

    const {data, error} = await apiFetch({
      url: `preferences/getAllAnchors?cToken=${token}&employeeId=${employeeId}&startDate=${startDate}&endDate=${endDate}${
        teamId ? '&teamId=' + teamId : ''
      }${managedTeamId ? '&managedTeamId=' + managedTeamId : ''}`,
    });
    if (data) {
      dispatch(getAnchorsSuccess(data));
    }
    if (error) {
      dispatch(getAnchorsFail(error));
    }
    dispatch(removeLoading());
    error && console.log('-----getAllAnchors--error', error);
  };

export const addAnchor = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(addAnchorRequest(payload));
  const token = await getToken();

  const {data, error} = await apiFetch({
    url: `preferences/addAnchor?cToken=${token}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(addAnchorSuccess(data));
  }
  if (error) {
    dispatch(addAnchorFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----addAnchor--error', error);
};

export const changeAnchor = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(changeAnchorRequest());
  const token = await getToken();

  const {data, error} = await apiFetch({
    url: `preferences/changeAnchor?cToken=${token}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(changeAnchorSuccess(data));
  }
  if (error) {
    dispatch(changeAnchorFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----changeAnchor--error', error);
};

export const deleteAnchor = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(deleteAnchorRequest(payload));
  const token = await getToken();
  console.log('deleteAnchor', payload);

  const {data, error} = await apiFetch({
    url: `preferences/deleteAnchor?cToken=${token}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(deleteAnchorSuccess(data));
  }
  if (error) {
    dispatch(deleteAnchorFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----deleteAnchor--error', error);
};

export const getExceptionReasons = () => async dispatch => {
  dispatch(showLoading());
  dispatch(getExceptionReasonsRequest());
  const token = await getToken();

  const {data, error} = await apiFetch({
    url: `preferences/anchor/getExceptionReasons?cToken=${token}`,
  });
  if (data) {
    dispatch(getExceptionReasonsSuccess(data));
  }
  if (error) {
    dispatch(getExceptionReasonsFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----getExceptionReasons--error', error);
};

export const requestAnchorException = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(requestAnchorExceptionRequest());
  const token = await getToken();

  const {data, error} = await apiFetch({
    url: `preferences/requestAnchorException?cToken=${token}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(requestAnchorExceptionSuccess(data));
  }
  if (error) {
    dispatch(requestAnchorExceptionFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----requestAnchorException--error', error);
};

export const cancelRequestException = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(cancelRequestExceptionRequest());
  const token = await getToken();

  const {data, error} = await apiFetch({
    url: `preferences/cancelAnchorRequestException?cToken=${token}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(cancelRequestExceptionSuccess(data));
  }
  if (error) {
    dispatch(cancelRequestExceptionFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----cancelRequestException--error', error);
};
