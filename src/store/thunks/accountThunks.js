import {apiFetch} from '../../api/apiFetch';
import {setToken} from '../../api/asyncStorage';
import {
  getCodeFail,
  getCodeRequest,
  getCodeSuccess,
  loginUserSuccess,
} from '../actions/accountActions';
import {removeLoading, showLoading} from '../actions/loadingActions';
import {store} from '../store';

export const getCode =
  ({email, cb}) =>
  async dispatch => {
    dispatch(getCodeSuccess(email));
    dispatch(showLoading());
    dispatch(getCodeRequest());
    const {data, error} = await apiFetch({
      url: `employees/signIn?email=${email}`,
    });

    dispatch(getCodeSuccess(email));
    dispatch(removeLoading());
    cb && cb();
  };

export const signInWithCode =
  ({code, cb}) =>
  async dispatch => {
    dispatch(showLoading());
    const email = store.getState().account.email;

    // const {data, error} = await apiFetch({
    //   url: `employees/authenticate?email=${email}&code=${code}`,
    // });
    dispatch(removeLoading());
    // dispatch(loginUserSuccess(data));
    // await setToken(data);

    dispatch(loginUserSuccess('Ark7yJDmsSOQtSS8wjEMk8vRms05KmQEfJoEBNeYoKI'));
    await setToken('Ark7yJDmsSOQtSS8wjEMk8vRms05KmQEfJoEBNeYoKI');
    cb && cb();
  };
