import {apiFetch} from '../../api/apiFetch';
import {
  getEmployeeFail,
  getEmployeeRequest,
  getEmployeeSuccess,
  getAllInterestsRequest,
  getAllInterestsSuccess,
  getAllInterestsFail,
} from '../actions/employeeActions';
import {removeLoading, showLoading} from '../actions/loadingActions';
import {store} from '../store';
import {getToken} from '../../api/asyncStorage';

export const getEmployeeData = () => async dispatch => {
  dispatch(showLoading());
  dispatch(getEmployeeRequest());
  const token = await getToken();
  const email = store.getState().account.email || 'bbrutman@outbrain.com';

  const {data, error} = await apiFetch({
    url: `employees/getEmployeeData?email=${email}&cToken=${token}`,
  });
  if (data) {
    dispatch(getEmployeeSuccess(data));
  }
  if (error) {
    dispatch(getEmployeeFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----getEmployeeDataThunk--error', error);
};

export const getAllInterests = () => async dispatch => {
  dispatch(showLoading());
  dispatch(getAllInterestsRequest());
  const token = await getToken();
  const {employeeId} = store.getState().employee.employeeData;

  const {data: сompanyInterests, errorCompanyInterest} = await apiFetch({
    url: `preferences/getCompanyInterests?cToken=${token}`,
  });
  const {data: employeeInterests, errorEmployeeInterests} = await apiFetch({
    url: `preferences/getEmployeeInterests?cToken=${token}&employeeId=${employeeId}`,
  });
  if (сompanyInterests && employeeInterests) {
    const arrInterestsObjects = сompanyInterests.map(item => ({
      text: item,
      isChecked: employeeInterests.interests.includes(item),
      id: +(Math.random() * 100000).toFixed(),
    }));
    employeeInterests.personalInterests.forEach(inter => {
      arrInterestsObjects.push({
        text: inter,
        isChecked: true,
        id: +(Math.random() * 100000).toFixed(),
      });
    });
    dispatch(
      getAllInterestsSuccess({
        all: arrInterestsObjects,
        сompanyInterests,
        employeeInterests,
      }),
    );
  }
  if (errorCompanyInterest || errorEmployeeInterests) {
    dispatch(
      getAllInterestsFail(errorCompanyInterest || errorEmployeeInterests),
    );
    console.log(
      '------getAllInterests',
      errorCompanyInterest || errorEmployeeInterests,
    );
  }
  dispatch(removeLoading());
};

export const updateEmployeeInterests = (interests, cb) => async dispatch => {
  dispatch(showLoading());
  const token = await getToken();
  const {employeeId} = store.getState().employee.employeeData;

  const {error} = await apiFetch({
    url: `preferences/updateEmployeeInterests?cToken=${token}&employeeId=${employeeId}`,
    method: 'POST',
    body: JSON.stringify(interests),
  });

  // console.log('updateEmployeeInterests', data);
  error && console.log('updateEmployeeInterests------error', error);
  dispatch(removeLoading());
  cb();
};
