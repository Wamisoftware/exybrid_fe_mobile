import {apiFetch} from '../../api/apiFetch';
import {removeLoading, showLoading} from '../actions/loadingActions';
import {
  getColleaguesFail,
  getColleaguesRequest,
  getColleaguesSuccess,
  getRelationsRequest,
  getRelationsSuccess,
  getRelationsFail,
  setRelationsRequest,
  setRelationsSuccess,
  setRelationsFail,
} from '../actions/colleaguesActions';
import {getToken} from '../../api/asyncStorage';
import {store} from '../store';

export const getAllColleagues = () => async dispatch => {
  dispatch(showLoading());
  dispatch(getColleaguesRequest());
  const token = await getToken();
  const {data, error} = await apiFetch({
    url: `employees/getAll?cToken=${token}`,
  });
  if (data) {
    dispatch(getColleaguesSuccess(data));
  }
  if (error) {
    dispatch(getColleaguesFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----getAllColleagues--error', error);
};

export const getRelations = () => async dispatch => {
  dispatch(showLoading());
  dispatch(getRelationsRequest());
  const token = await getToken();
  const {employeeId} = store.getState().employee.employeeData;
  const {data, error} = await apiFetch({
    url: `preferences/getRelations?cToken=${token}&employeeId=${employeeId}`,
  });
  if (data) {
    dispatch(getRelationsSuccess(data));
  }
  if (error) {
    dispatch(getRelationsFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----getRelations--error', error);
};

export const setRelations = payload => async dispatch => {
  dispatch(showLoading());
  dispatch(setRelationsRequest());
  const token = await getToken();
  const {employeeId} = store.getState().employee.employeeData;
  const {data, error} = await apiFetch({
    url: `preferences/setOrUpdateRelations?cToken=${token}&employeeId=${employeeId}`,
    method: 'POST',
    body: JSON.stringify(payload),
  });
  if (data) {
    dispatch(setRelationsSuccess(data));
  }
  if (error) {
    dispatch(setRelationsFail(error));
  }
  dispatch(removeLoading());
  error && console.log('-----setRelations--error', error);
};
