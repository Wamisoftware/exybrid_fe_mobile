import * as types from '../actionTypes';

export const getColleaguesRequest = () => ({
  type: types.GET_ALL_COLLEAGUES_REQUEST,
});
export const getColleaguesSuccess = payload => ({
  type: types.GET_ALL_COLLEAGUES_SUCCESS,
  payload,
});
export const getColleaguesFail = payload => ({
  type: types.GET_ALL_COLLEAGUES_FAIL,
  payload,
});

export const getRelationsRequest = () => ({
  type: types.GET_RELATIONS_REQUEST,
});
export const getRelationsSuccess = payload => ({
  type: types.GET_RELATIONS_SUCCESS,
  payload,
});
export const getRelationsFail = payload => ({
  type: types.GET_RELATIONS_FAIL,
  payload,
});

export const setRelationsRequest = () => ({
  type: types.SET_RELATIONS_REQUEST,
});
export const setRelationsSuccess = payload => ({
  type: types.SET_RELATIONS_SUCCESS,
  payload,
});
export const setRelationsFail = payload => ({
  type: types.SET_RELATIONS_FAIL,
  payload,
});
