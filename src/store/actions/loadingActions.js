import {SHOW_LOADING, REMOVE_LOADING} from '../actionTypes';

export const showLoading = () => ({
  type: SHOW_LOADING,
});

export const removeLoading = () => ({
  type: REMOVE_LOADING,
});
