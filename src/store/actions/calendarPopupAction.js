import * as types from '../actionTypes';

export const setAddPopup = payload => ({
  type: types.SET_ADD_POPUP,
  payload,
});
export const setChangePopup = payload => ({
  type: types.SET_CHANGE_POPUP,
  payload,
});
export const setExistingPopup = payload => ({
  type: types.SET_EXISTING_POPUP,
  payload,
});

export const setDataToPopup = payload => ({
  type: types.SET_DATA_TO_POPUP,
  payload,
});

export const setAllAnchors = payload => ({
  type: types.SET_ALL_ANCHORS,
  payload,
});
