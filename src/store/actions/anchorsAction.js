import * as types from '../actionTypes';

export const getAnchorsRequest = () => ({
  type: types.GET_ALL_ANCHORS_REQUEST,
});
export const getAnchorsSuccess = payload => ({
  type: types.GET_ALL_ANCHORS_SUCCESS,
  payload,
});
export const getAnchorsFail = payload => ({
  type: types.GET_ALL_ANCHORS_FAIL,
  payload,
});

export const addAnchorRequest = () => ({
  type: types.ADD_ANCHOR_REQUEST,
});
export const addAnchorSuccess = payload => ({
  type: types.ADD_ANCHOR_SUCCESS,
  payload,
});
export const addAnchorFail = payload => ({
  type: types.ADD_ANCHOR_FAIL,
  payload,
});

export const changeAnchorRequest = () => ({
  type: types.CHANGE_ANCHOR_REQUEST,
});
export const changeAnchorSuccess = payload => ({
  type: types.CHANGE_ANCHOR_SUCCESS,
  payload,
});
export const changeAnchorFail = payload => ({
  type: types.CHANGE_ANCHOR_FAIL,
  payload,
});

export const getExceptionReasonsRequest = () => ({
  type: types.GET_EXCEPTION_REASONS_REQUEST,
});
export const getExceptionReasonsSuccess = payload => ({
  type: types.GET_EXCEPTION_REASONS_SUCCESS,
  payload,
});
export const getExceptionReasonsFail = payload => ({
  type: types.GET_EXCEPTION_REASONS_FAIL,
  payload,
});

export const requestAnchorExceptionRequest = () => ({
  type: types.REQUEST_ANCHOR_EXCEPTION_REQUEST,
});
export const requestAnchorExceptionSuccess = payload => ({
  type: types.REQUEST_ANCHOR_EXCEPTION_SUCCESS,
  payload,
});
export const requestAnchorExceptionFail = payload => ({
  type: types.REQUEST_ANCHOR_EXCEPTION_FAIL,
  payload,
});

export const cancelRequestExceptionRequest = () => ({
  type: types.CANCEL_REQUEST_EXCEPTION_REQUEST,
});
export const cancelRequestExceptionSuccess = payload => ({
  type: types.CANCEL_REQUEST_EXCEPTION_SUCCESS,
  payload,
});
export const cancelRequestExceptionFail = payload => ({
  type: types.CANCEL_REQUEST_EXCEPTION_FAIL,
  payload,
});

export const deleteAnchorRequest = payload => ({
  type: types.DELETE_ANCHOR_REQUEST,
  payload,
});
export const deleteAnchorSuccess = payload => ({
  type: types.DELETE_ANCHOR_SUCCESS,
  payload,
});
export const deleteAnchorFail = payload => ({
  type: types.DELETE_ANCHOR_FAIL,
  payload,
});
