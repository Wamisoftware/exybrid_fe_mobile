import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_LOGOUT_ACCOUNT,
  SET_ACCOUNT_TOKEN,
  GET_CODE_REQUEST,
  GET_CODE_SUCCESS,
  GET_CODE_FAIL,
} from '../actionTypes';

export const getCodeRequest = () => ({
  type: GET_CODE_REQUEST,
});

export const getCodeSuccess = email => ({
  type: GET_CODE_SUCCESS,
  payload: email,
});

export const getCodeFail = error => ({
  type: GET_CODE_FAIL,
  payload: error,
});

export const logoutAccount = () => ({
  type: USER_LOGOUT_ACCOUNT,
});

export const loginUserRequest = () => ({
  type: USER_LOGIN_REQUEST,
});

export const loginUserSuccess = token => ({
  type: USER_LOGIN_SUCCESS,
  payload: token,
});

export const loginUserFail = error => ({
  type: USER_LOGIN_FAIL,
  payload: error,
});

export const setAccountToken = token => ({
  type: SET_ACCOUNT_TOKEN,
  payload: token,
});
