import * as types from '../actionTypes';

export const getEmployeeRequest = () => ({type: types.GET_EMPLOYEE_REQUEST});
export const getEmployeeSuccess = payload => ({
  type: types.GET_EMPLOYEE_SUCCESS,
  payload,
});
export const getEmployeeFail = payload => ({
  type: types.GET_EMPLOYEE_FAIL,
  payload,
});

export const getAllInterestsRequest = () => ({
  type: types.GET_ALL_INTERESTS_REQUEST,
});
export const getAllInterestsSuccess = payload => ({
  type: types.GET_ALL_INTERESTS_SUCCESS,
  payload,
});
export const getAllInterestsFail = payload => ({
  type: types.GET_ALL_INTERESTS_FAIL,
  payload,
});
